Lien vers le site : https://webetu.iutnc.univ-lorraine.fr/~vermande6u/mywishlist/

Installer l'application :

	- Cloner le depot git sur votre machine et le rendre accessible à votre serveur.
	- Installer les bibliothèques nécessaires grâce au composer (composer install)
	- Installer une base de données mysql et y lancer le script mywishlist.sql fourni dans le dossier src/
	- Si besoin, modifer le fichier .htaccess pour adapter la RewriteBase

Choix effectués :

	- Les messages publiques pour les listes sont anonymes et visibles en bas de la liste.

	- Chaque Item à une image par défault nommé random.jpg, si on supprime l'image actuelle ou un lien externe
	  l'image sera de nouveau random.jpg.

	- On modifie les images liés à un item quand on est sur la page de description et de l'item ET qu'on est 
	  propriétaire de la Liste dont il provient.

	- Si on modife une liste, on doit impérativement compléter tout les champs pour valider la modification.

	- Une Liste publique n'apparait dans la section liste publique que si on EST PAS propriétaire de la liste.
	  Dans le cas contraire elle apparaitra dans la section "Listes de souhait perso".

	- On ne peut pas créer de Liste ou modifier une liste pour que sa date d'expiration soit la date courante.
	  Pour faire qu'une liste de souhait expire le jour courant il faut que le propriétaire de cette derniere 
	  le demande explicitement dans la page de description de la liste en utilisant le bouton "faire expirer la liste".

	- Une liste est privée et ne peut être consultée que si on a l'adresse de cette derniere que si on a le 
	  lien pour y acceder. Une liste devient publique et apparait donc dans la liste des listes publiques à 
	  partir du moment ou son propiétaire appuie sur le boutton "publier la liste" sur la page de descrition.
	  
	- On ne peut plus modifier un item une fois que celui ci est réservé.
	
	- On ne peut pas dépublier une liste publique.

