<?php
namespace mywishlist\vue;

use mywishlist\models\Item;
use mywishlist\models\Liste;

/**
 * Classe permettant d'afficher les pages liees aux personnes visitant le site.
 *
 * Classe permettant d'afficher les pages liees aux personnes visitant le site,
 * autrement dit regardant une page ne concernant pas la creation ou la modification d'une liste,
 * uniquement la visite et la participation.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class VueParticipant extends Vue{
	
	const PageDeBase = 0, PageItem = 1, PageListes = 2, PageContenuListe = 3, PageConnexion = 4, PageInscription = 5, ContenuInaccessible = 6; 

	/**
	*	Méthode principale de rendu de la classe VueCreateur
	*	Elle prend en paramètre un sélecteur qui va définir la page que l'on souhaite afficher
	*	Le sélecteur doit faire partie des constantes définies dans la classe
	*	Ajoute le contenu dans la base html et l'affiche
	*
	*	@param int[$selecteur]
	*	@domain public
	*/
	public function render($selecteur){
		
		switch($selecteur){
			case self::PageDeBase:
				$content = $this->PageDeBase();
				break;
			case self::PageItem:
				$content = $this->afficherItem();
				break;
			case self::PageListes:
				$content = $this->afficherListes();
				break;
			case self::PageContenuListe:
				$content = $this->afficherContenuListe();
				break;
			case self::PageConnexion:
				$content = $this->afficherPageConnexion();
				break;
			case self::PageInscription:
				$content = $this->afficherPageInscription();
				break;
			case self::ContenuInaccessible:
				$content = $this->afficherErreur();
				break;
			default:
				$content = "<p>Par défaut</p>";
				break;
			// Un case par affichage de vue
		}
		$html = $this->baseHtml($content);

		echo $html;
	}	
	
	/**
	*	Méthode générant le texte html correspondant à la page d'accueil de d'un utilisateur non connecté
	*	Il a peut uniquement accéder aux listes publiques
	*
	*	@domain public
	*/
	private function PageDeBase(){
		
		$urlListesPersos = \Slim\Slim::getInstance()->urlFor('afficher_listes_persos') ;
		$urlListesPubliques = \Slim\Slim::getInstance()->urlFor('afficher_listes_publiques');

		$res = <<<END
	<div class="container-fluid centered">
		<div class="row">
			<div class="col-12">
				<h1>Bienvenue sur myWishList</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1 col-lg-1 col-xl-1"></div>
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
				<a class="btn btn-primary form-control" href="$urlListesPubliques" >Listes de souhaits publiques</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
				<a class="btn btn-secondary form-control" disabled>Vous devez être connecté pour créer une liste</a>
			</div>
		</div>
	</div>
END;
		return $res;
	}


	/**
	*	Méthode générant le code html pour l'affichage en détail d'un item d'une liste publique ou dont il a le token
	*	L'utilisateur peut réserver l'item, ou participer à la cagnotte si elle est ouverte
	*
	*	@domain public
	*/
	private function afficherItem(){ // Case 1
		$app = \Slim\Slim::getInstance();
		$token = "";
		if(!is_null($app->request->get('token'))){
			$token = "?token=".$app->request->get('token');
		}

		$item = $this->param;
		
		$rootListes = $app->urlFor('afficher_liste', ['no'=>$item->liste_id]);

		$reservationHTML = "";
		if($item->cagnotte == 0){//l'item peut être réservé par une seule personne
			if($item->estReserve()){
				$nom = $item->reservation()->nom;
				$reservationHTML = <<<END
					<p>Item réservé par $nom</p>
END;
			}
			else{
				$url = $app->urlFor('reserver_item', ['id'=>$item->id]).$token;
				$baliseNomReserveur = ""; //balise du nom du reserveur, set uniquement si le reserveur n'est pas quelqu'un de connecté
				if (!isset($_SESSION['profil'])) //si la personne est connectée
				{
					$nomReserveurParDefaut = "";
					if (isset( $_COOKIE[ 'nom_reserveur' ] )) {
						$nomReserveurParDefaut = $_COOKIE[ 'nom_reserveur' ];
					}
					$baliseNomReserveur = "<p>Votre nom : <input type=\"text\" name=\"nomParticipant\" value=\"$nomReserveurParDefaut\" required></p>";
				}
				//sinon si la personne est connectée on ne lui donne pas la possibilité de donner son nom, puisqu'elle réservera en son nom de compte
				$reservationHTML = <<<END
				<p>Voulez-vous réserver cet item ?</p>
					<form id="ReserverUnItem" method="post" action=$url>
						$baliseNomReserveur
						<p>Votre mot :</p>
						<p><input type="text" name="texteReservation" value=""></p>
						
						<button class="btn btn-primary" type="submit" name="reserver_Item" value="reserver">Réserver</button>
					</form>
END;
			}
		}
		else { //une cagnotte a été ouverte pour l'item
			if($item->estReserve()){
				$reservationHTML = <<<END
					<p> Participants à la cagnotte : 
END;
				foreach ($item->reservations()->get() as $participationCagnotte) {		
					$reservationHTML .= $participationCagnotte->nom." ";
				}
				$reservationHTML .= "</p>";
			}
			else{
				$reservationHTML = <<<END
					<h3>Soyez le premier à participer !</h3>
END;
			}

			if($item->montantCagnotte() < $item->tarif){
				$url = $app->urlFor('participer_cagnotte', ['id'=>$item->id]).$token;
				$baliseNomReserveur = ""; //balise du nom du reserveur, set uniquement si le reserveur n'est pas quelqu'un de connecté
				if (!isset($_SESSION['profil'])) //si la personne n'est connectée
				{
					$nomReserveurParDefaut = "";
					if (isset( $_COOKIE[ 'nom_reserveur' ] )) {
						$nomReserveurParDefaut = $_COOKIE[ 'nom_reserveur' ];
					}
					$baliseNomReserveur = <<<END
					<label for="nomParticipant">Votre nom</label>
								<input class="form-control" id="nomParticipant" type="text" name="nomParticipant" value="$nomReserveurParDefaut" required>
END;
				}
				//sinon si la personne est connectée on ne lui donne pas la possibilité de donner son nom, puisqu'elle réservera en son nom de compte
				$reste = $item->tarif - $item->montantCagnotte();
				$reservationHTML .= <<<END
				<div class="row">
					<div class="col">
						<h5>Reste à payer : $reste €</h5>
						<form id="participerCagnotte" method="post" action=$url>
							<div class="form-group" style="text-align:center;">
								$baliseNomReserveur
								<p>Votre mot :</p>
								<p><input type="text" name="texteReservation" value=""></p>
							</div>
							<div class="form-group" style="text-align:center;">
								<label for="montantParticipation">Montant de la participation</label>
								<input class="form-control" type="number" min="1" id="montantParticipation" name="montantParticipation" value="0" required>
							</div>
							<button class="btn btn-primary" type="submit" name="reserver_Item" value="reserver">réserver</button>
						</form>
					</div>
				</div>
END;
			}
		}


		//Gestion des images
		$img = $item->img;
		if( filter_var( $img, FILTER_VALIDATE_URL)){
			$rootIMG = $img;
		}
		else{
			$rootIMG = \Slim\Slim::getInstance()->request()->getRootUri();
			$rootIMG .= "/img/".$img;
		}
		
		$boutonRetour = $this->boutonRetour($rootListes, "Retour à la liste");
		$res = <<<END
			<div class="container centered">
				<h1><span class="badge badge-primary">$item->nom</span></h1>
				<p>$item->descr</p>
				<p>Valeur : $item->tarif €</p>
				<img alt="$img" src="$rootIMG" >
				$reservationHTML

				$boutonRetour
			</div>
END;
		return $res;
	}




	/**
	*	Méthode générant le texte html des listes publiques
	*
	*	@domain public
	*/
	private function afficherListes(){
		$urlRetour = \Slim\Slim::getInstance()->urlFor('route_defaut');
		$boutonRetour = $this->boutonRetour($urlRetour, "Retour à l'accueil");
		$res = <<<END
		<div class="container">
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6 centered">
					<h1>Listes publiques</h1>
				<div class="col-3"></div>
				</div>
			</div>
END;
		if(count($this->param) == 0){//Il n'y a aucune liste publique (ou elles sont toutes expirees)
			$res .= <<<END
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6 centered">
					<h2>Il n'y a aucune liste publique</h2>
				</div>
				<div class="col-3"></div>
			</div>
END;
		}
		else{
			$res.= $this->blocListes($this->param);	
		}
		$res.= <<<END
			$boutonRetour
		</div>
END;
		return $res;
	}
	
	/**
	*	Méthode générant le texte html pour l'affichage en détail d'une liste publique ou dont l'utilisateur a le token
	*	L'utilisateur voit les messages laissés par les utilisateur et peut en ajouter un
	*
	*	@domain public
	*/
	private function afficherContenuListe(){ // Case 3
				
		// PARTIE COMMUNE
		$liste = $this->param[0];
		$objets = $this->param[1];
		
		$texteListe = $this->enteteListe($liste);
		$ajouterToken = isset($_GET['token']);
		//si la liste consultée à nécessité un token, on l'ajoute aux liens des items
		$texteItems = $this->blocItems($objets, 1, $ajouterToken);

		$urlRetour = \Slim\Slim::getInstance()->urlFor('afficher_listes_publiques');
		$boutonRetour = $this->boutonRetour($urlRetour, "Retour aux listes de souhait publiques");

		$urlMessage = \Slim\Slim::getInstance()->urlFor('ajouter_Message_liste',['id'=>$liste->no]);
		$app = \Slim\slim::getInstance();
		if(!is_null($app->request()->get('token'))){
			$urlMessage .= "?token=".$app->request->get('token');
		}
		
		$MessagesListe = $liste->toutMessageListe();
		$user = $liste->utilisateur()->first();
		$res = <<<END
			<div class="container">
				<div class="centered">
					$texteListe
					<div class="h3">
						Créée par $user->nom
					</div>
					$texteItems
				</div>

				
				<div>
					$MessagesListe
				</div>
				
				<div class="row">
					<div class="col-4"></div>
					<div class="col-4 centered">
						<form id="ajouterMessage" method="post" action=$urlMessage>
							<div class="form-group">
								<label for="motMessage">Entrez votre message ci dessous</label>
								<input class="form-control" type="text" id="motMessage" name="inputMessage" maxlength="200" size="10">
							</div>
							<button class="btn btn-primary" type="submit" name="valider_envoit_message">Valider</button>
						</form>
					</div>
					<div class="col-4"></div>
				</div>
				
				$boutonRetour
			</div>
END;
	
		return $res;
	}


	/**
	*	Méthode générant le texte html de la page de connexion
	*	l'utilisateur doit indiquer son email et son mdp
	*
	*	@domain public
	*/
	public function afficherPageConnexion(){
		$msg = "";		
		if(isset($_COOKIE['connexion_ratee'])){
			$msg = $_COOKIE['connexion_ratee'];
		}
		$urlRet = \Slim\Slim::getInstance()->urlFor('route_defaut');
		$boutonRetour = $this->boutonRetour($urlRet, "Retour à la page d'accueil");
		$res =  <<<END
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 col-md-3"></div>
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 col-lg-4">
				<form id="connexion" method="post">
					$msg
					<div class="form-group"> 
						<label for="email">Email</label>
						<input class="form-control" id="email" placeholder="email" type="text" name="email" required>
					</div>
		    		
		    		<div class="form-group">
						<label for="mdp">Mot de passe</label>
						<input class="form-control" id="mdp" type="password" name="mdp" required>
					</div>
					<button class="btn btn-primary" type="submit" name="valider_connexion">Valider</button>
				</form>
			</div>
			<div class="col-2"></div>
		</div>
		$boutonRetour
	</div>

END;
		return $res;
	}


	/**
	*	Méthode générant le texte html de la page d'inscription
	*	l'utilisateur doit indiquer son nom, prenom, email et un mdp
	*
	*	@domain public
	*/
	public function afficherPageInscription(){
		$msg = "";		
		if(isset($_COOKIE['erreur_email'])){
			$msg = $_COOKIE['erreur_email'];
		}

		$urlRet = \Slim\Slim::getInstance()->urlFor('route_defaut');
		$boutonRetour = $this->boutonRetour($urlRet, "Retour à la page d'accueil");
	
		$res =  <<<END
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-md-3"></div>
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 col-lg-4">
					<form id="inscription" method="post">
						$msg
						<div class="form-group">
							<label class="form-label">Nom</label>
							<input class="form-control" type="text" name="nom" required>
						</div>
						<div class="form-group">
							<label>Prenom</label>
							<input class="form-control" type="text" name="prenom" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input class="form-control" type="text" name="email" required>
						</div>
						<div class="form-group">
							<label>Mot de passe</label>
							<input class="form-control" type="password" name="mdp" required>
						</div>
						<button class="btn btn-primary" type="submit" name="valider_connexion">Valider</button>
					</form>
				</div>
				<div class="col-2"></div>
			</div>
			$boutonRetour
		</div>
END;
		
		return $res;

		}
	
	
}