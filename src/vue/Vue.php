<?php

namespace mywishlist\vue;

/**
 *	Classe mère des classes VueCreateur et VueParticipant
 *
 *	Classe mère des classes VueCreateur et VueParticipant
 *	Elle regroupe toutes les méthodes qu'elles ont en commun
 *	Contien également le texte html "global" du site web dans le quel on ajoute le contenu
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
abstract class Vue{

	protected $param;
	
	public function __construct($p){
		$this->param = $p;
	}


	/**
	* Methode qui retourne une page HTML complete.
	*
	* Methode qui retourne une page HTML complete en associant une
	* base et un contenue.
	*
	* Structure html de base du site avec header, footer et le contenu en paramètre à ajouter entre les deux
	* base et un contenue créer à partir du parametres content de la fonction.
	* Contenue HTML passé en parametres (string)
	* Le header contient une division dédiée à la connexion/inscription/déconnexion/etc..

	*
	*	@param string[$content], le contenu à mettre au centre de la page 
	*	@domain public
	*/
	public function baseHtml($content){
		$rootCSS = \Slim\Slim::getInstance()->request()->getRootUri();
		$rootCSS.= "/src/css/css_principal.css";
		$rootBootstrap = \Slim\Slim::getInstance()->request()->getRootUri()."/src/css/bootstrap.min.css";
		$pathJS =  \Slim\Slim::getInstance()->request()->getRootUri()."/src/js/script.js";
		$connexion = $this->blocConnexion();
		$html = <<<END
					<!DOCTYPE html>
					<html>
						<head>
							<title>My Wish Liste</title>
								<link rel="stylesheet" type="text/css" href="$rootBootstrap">
								<link rel="stylesheet" type="text/css" href="$rootCSS">
							<meta charset="UTF-8">
						</head>
						
						<body>
							
							<header class="container-fluid title">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
										<h1 class="centered">Bienvenue sur MyWishList</h1>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
										$connexion
									</div>
								</div>
							</header>	
							
							$content


							<footer class="container-fluid footer">
								<div class="row">
									<div class="col-4 centered">
										Hodot Arthur
									</div>
									<div class="col-4 centered">
										Risacher Jeremy
									</div>
									<div class="col-4 centered">
										Vermandé Valentin
									</div>
								</div>
							</footer>
						
							<script type="text/javascript" src="$pathJS"></script>
						</body>
					</html>
END;
		return $html;
	}



	/**
	*	Méthode générant le code html de la page d'erreur lorsqu'un utilisateur essaye d'acceder à une page inexistante
	*
	*	@domain public
	*/
	protected function afficherErreur(){
		$app = \Slim\Slim::getInstance();
		$url = $app->urlFor('route_defaut');
		$boutonRetour = $this->boutonRetour($url, "Accès à la page d'acceuil");
		return <<<END
		<div class="container">
			<div class="row centered">
				<div class="col">
					<h1><span class="badge badge-warning">Vous n'avez pas accès à cette page</span></h1>
				</div>
			</div>
			$boutonRetour
		</div>
END;
	}

	/**
	*	Methode permettant de générer l'html des listes pour les afficher dans des cartes
	*	Il y a 3 listes par ligne au maximum
	*	Un carte est composée du titre de la liste, sa date d'expiration et le nom du créateur
	*
	*	@param array[$listes], le tableau des Listes à afficher
	*	@domain public
	*/
	protected function blocListes($listes){
		$nbListes = count($listes);
		$res = "";
		$nbLignesNormales = intdiv($nbListes, 4);
		$nbListesRestantes = $nbListes%4;
		for($i = 0; $i < $nbLignesNormales; $i++){
			$res .= <<<END
			<div class="row">
END;
			for($j = 0; $j < 4; $j++){
				$list = $listes[$i*4+$j];
				$carte = $list->asCard();
				$res.= <<<END
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
					$carte
				</div>
END;
			}
				$res .= <<<END
			</div>
END;
		}
		if($nbListesRestantes > 0){
			switch($nbListesRestantes){
				case 1:
					$col = <<<END
					<div class="col-md-3 col-lg-4 col-xl-4"></div>
END;
					break;
				case 2:
					$col = <<<END
					<div class="col-lg-2 col-xl-2"></div>
END;
					break;
				case 3:
					$col = "";
					break;
			}
			$res .= <<<END
			<div class="row">
				$col
END;
			for($j = 0; $j < $nbListesRestantes; $j++){
				$list = $listes[$nbLignesNormales*4+$j];
				$res.= <<<END
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
END;
				$res.= $list->asCard()."</div>";
			}
			$res .= <<<END
			</div>
END;
		}

		return $res;
	}


	/*
	* afficherReservation :
	* 1 : affichage de la liste par un utilisateur
	* 2 : affichage pour le createur (lorque la liste est expiree)
	*
	*	@domain public
	*/
	protected function blocItems($objets, $afficherReservation, $ajouterToken = false){
		if(count($objets) == 0){ //LISTE SANS ITEM
			$res = <<<END
			<div class="container" id="bloc-items">
				<div class="row">
					<div class="col centered">
						<h6>Cette liste ne contient encore aucun objet</h6>
					</div>
				</div>
			</div>
END;
		}
		else{
			$res = <<<END
			<div class="container centered" id="bloc-items">
				<ul class="list-group">		 					
END;
			foreach($objets as $obj) {
				$nom = $obj->nom;
				$id = $obj->id;
				$lien = \Slim\Slim::getInstance()->urlFor('afficher_item',['id'=>$id]);
				if($ajouterToken){
					$lien .= "?token=".$_GET['token'];
				}
				$rootIMG = \Slim\Slim::getInstance()->request()->getRootUri();
				
				$txtReservation = $obj->texteReservation($afficherReservation);

				//Gestion des images
				if ($obj->img != null) {
					$img = $obj->img;
					if( filter_var( $img, FILTER_VALIDATE_URL)){
						$rootIMG = $img;
					}
					else{
						$rootIMG = \Slim\Slim::getInstance()->request()->getRootUri();
						$rootIMG .= "/img/".$img;
					}
				}
				else{
					$rootIMG.='/img/random.jpg';	
				}
				$res .= <<<END
					<li class="list-group-item">
						<a class="link-list" href="$lien">
							<img src="$rootIMG" alt="$obj->nom" height="42" width="42">
							<h5>$nom</h5>
							$txtReservation
						</a>
					</li>
END;
			}
			$res .= <<<END
				</ul>
			</div>
END;
		}
		return $res;
	}

	/**
	*	Methode générant le texte html pour retourner à la page précédente
	*	à mettre à la fin de la page
	*
	*	@param string[$url], l'url où mène le bouton
	*	@param string[$nomBouton], le nom du bouton
	*	@domain public
	*/
	protected function boutonRetour($url, $nomBouton = "Retour à la page précedente"){
		if(isset($_GET['token'])){
			$url .= '?token='.$_GET['token'];
		}
		return <<<END
		<div class="row">
			<div class="col-1"></div>
			<div class="col">
				<a id="bouton-retour" class="btn btn-primary" href="$url">$nomBouton</a>
			</div>
		</div>
END;
	}

	/**
	*	Méthode générant le texte html d'un entête de liste
	*	ce qui comprend le titre, la date d'expiration et sa description
	*
	*	@param Liste[$liste]
	*	@domain public
	*/
	protected function enteteListe($liste){
		$titre = $liste->titre;
		$expiration = $liste->expiration;
		$expiration = substr($expiration, 5, 2)."/".substr($expiration, 8)."/".substr($expiration, 0, 4);
		$description = $liste->description;
		return <<<END
			<div class="container">
				<div class="row">
					<div class="col">
						<h1><span class="badge badge-secondary">$titre</span></h1>
						<h3>Expire le $expiration.</h3>
						<p style="word-break: break-all;">$description</p>
					</div>
				</div>
			</div>
END;
	}

	/**
	*	Methode générant le texte html du bloc de connexion dans le header
	*	si l'utilisateur n'est pas connecté, il y a un bouton pour se connecter et un pour s'inscrire
	*	si l'utilisateur est connecté, il y a un bouton de déconnexion, et un pour accéder aux modifications de compte
	*
	*	@domain public
	*/
	protected function blocConnexion(){
		if(isset($_SESSION['profil']['user_id'])){
			$user = \mywishlist\models\Utilisateur::find($_SESSION['profil']['user_id']);
			$rootDeconnection = \Slim\Slim::getInstance()->urlFor('deconnexion');
			$rootModification = \Slim\Slim::getInstance()->urlFor('modification_compte');
			return <<<END
			<div id="connexion">
				Connecté en tant que <br>
				<h4>$user->nom</h4>
				<form id="deconnexion" method="get">
					<div class="btn-group">
						<button type="submit" class="btn btn-light bouton-connexion" formaction=$rootDeconnection>Se deconnecter</button>
						<button type="submit" class="btn btn-secondary bouton-connexion" formaction=$rootModification>Modifier mes informations</button>
					</div>
				</form>
			</div>
END;
		}
		else{
			$rootConnection = \Slim\Slim::getInstance()->urlFor('connexion');
			$rootInscription = \Slim\Slim::getInstance()->urlFor('inscription');
			return <<<END
			<div id="connexion">
				<form id="sign" method="get">
					<div class="form-group">
						<button type="submit" class="btn btn-light form-control bouton-connexion" formaction=$rootConnection >Se connecter</button>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-light form-control bouton-connexion" formaction=$rootInscription >S'inscrire</button>
					</div>
				</form>
			</div>
END;
		}
	}



}