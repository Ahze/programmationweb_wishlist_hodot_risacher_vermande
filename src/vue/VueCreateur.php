<?php
namespace mywishlist\vue;


use mywishlist\models\Item;
use mywishlist\models\Liste;

/**
 * Classe permettant d'afficher des pages d'un createur de contenu.
 *
 * Classe permettant d'afficher des pages d'un createur de contenu.
 * Le createur de contenu a acces aux pages de creation et modification des listes et items
 * dont il est titulaire.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class VueCreateur extends Vue{


	const PageListes = 0, PageCreerListe = 1, PageModifierListe = 2, PageAjouterItem = 3, PageModifierItem = 4, PageContenuListe = 5, PageModifierImageItemOnline = 6, PageItem = 7, PageModifierImageItemInterne = 8, PageDeBase = 9, PageModifierCompte = 10, PageModifierMDP = 11;
	
	/**
	*	Méthode principale de rendu de la classe VueCreateur
	*	Elle prend en paramètre un sélecteur qui va définir la page que l'on souhaite afficher
	*	Le sélecteur doit faire partie des constantes définies dans la classe
	*	Ajoute le contenu dans la base html et l'affiche
	*
	*	@param int[$selecteur]
	*	@domain public
	*/
	public function render($selecteur)
	{
		//Faire un if pour voir si on est connécté
		switch($selecteur){
			case self::PageDeBase:
				$content = $this->PageDeBase();
				break;
			case self::PageListes :
				$content = $this->afficherListesPerso();
				break;
			case self::PageCreerListe:
				$content = $this->afficherCreationListe();
				break;
			case self::PageModifierListe:
				$content = $this->afficherModificationListe();
				break;
			case self::PageAjouterItem:
				$content =$this->afficherAjouterItemListe();
				break;
			case self::PageModifierItem:
				$content = $this->afficherModificationItem();
				break;
			case self::PageContenuListe:
				$content = $this->afficherContenuListe();
				break;
			case self::PageModifierImageItemOnline:	
				$content = $this->affichageImageItemOnline();
				break;
			case self::PageItem:
				$content = $this->afficherItem();
				break;
			case self::PageModifierImageItemInterne:
				$content = $this->affichageImageItemInterne();
				break;
			case self::PageModifierCompte:
				$content = $this->afficherPageModificationCompte();
				break;
			case self::PageModifierMDP:
				$content = $this->afficherPageModificationMDP();
				break;
			default:
				break;
		}

		$html = $this->baseHtml($content);

		echo $html;
	}
	
	/**
	*	Méthode générant le texte html correspondant à la page d'accueil de d'un utilisateur connecté
	*	Il a le choix d'accéder aux listes publiques qui ne lui appartiennent pas, de consulter ses listes personnelles ou de créer une liste
	*
	*	@domain public
	*/
	private function PageDeBase(){
		
		$urlListesPersos = \Slim\Slim::getInstance()->urlFor('afficher_listes_persos') ;
		$urlListesPubliques = \Slim\Slim::getInstance()->urlFor('afficher_listes_publiques');
		//On peut créer
		$urlCreerListe = \Slim\Slim::getInstance()->urlFor('creer_liste') ;
		$buttonCreerListe = <<<END
		<button type="submit" class="btn btn-primary" formaction="$urlCreerListe">Creer une liste</button>
END;
		//Et on a acces à SES listes 
		$buttonListesPersos = <<<END
		<button type="submit" class="btn btn-primary" formaction="$urlListesPersos">Listes de souhaits perso</button>
END;
		$res = <<<END
	<div class="container centered">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>Bienvenue sur myWishList</h1>
			
				<form>
					<button type="submit" class="btn btn-primary" formaction="$urlListesPubliques" >Listes de souhaits publiques</button>
					$buttonListesPersos
					$buttonCreerListe
				</form>
			</div>
		</div>
	</div>
END;
		return $res;
	}

	/**
	*	Méthode générant le texte html des listes personnelles, divisées en deux parties qui sont les listes en cours et les listes expirées
	*
	*	@domain public
	*/
	private function afficherListesPerso(){
		$res = <<<END
		<div class="container">
			<div class="row centered">
				<div class="col centered">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-primary" onclick="toggle('listes-en-cours')">Cacher/afficher listes en cours</button>
						<button type="button" class="btn btn-secondary" onclick="toggle('listes-expirees')">Cacher/afficher listes Expirées</button>
	  				</div>
	  			</div>
  			</div>
END;
  		$listesEnCours = array();
  		$listesExpirees = array();
  		foreach ($this->param as $list) {
  			if($list->estExpiree()){
  				$listesExpirees[] = $list;
  			}
  			else{
  				$listesEnCours[] = $list;
  			}
  		}

  		if(count($listesEnCours) > 0){
  			$res .= <<<END
	  			<div class="container" id="listes-en-cours">
  					<div class="row">
  						<div class="col-3"></div>
  						<div class="col-6">
  							<p class="h2 centered"> Listes en cours </p>
		  				</div>
		  				<div class="col-3"></div>
		  			</div>
END;
			$res.= $this->blocListes($listesEnCours);
			$res .= <<<END
				</div>
END;
  		}
  		if(count($listesExpirees) > 0){
			$res .= <<<END
				<div class="container" id="listes-expirees">
  					<div class="row">
  						<div class="col">
  							<p class="h2 centered"> Listes expirées </p>
		  				</div>
		  			</div>
END;
			$res.= $this->blocListes($listesExpirees);
			$res.= <<<END
				</div>
END;
		}
		$url = \Slim\Slim::getInstance()->urlFor('route_defaut');
		$res .= $this->boutonRetour($url, "Retour à la page d'accueil");
		return $res."</div>";
	}


	/**
	*	Méthode générant le texte html pour l'affichage en détail d'une liste appartenant à l'utilisateur
	*	L'utilisateur peut ajouter un item, faire expirer sa liste, modifier les informations de la liste ou la supprimer
	*	L'utilisateur a également accès aux objets faisant partie de la liste
	*
	*	@domain public
	*/
	private function afficherContenuListe(){ // Case 3
				
		$liste = $this->param[0];
		$objets = $this->param[1];
		
		$res = <<<END
		<div class="container centered">
END;
		$res .= $this->enteteListe($liste);
		$app = \Slim\Slim::getInstance();
		$no = $liste->no;
		$urlAjoutItem = $app->urlFor('ajouter_item_a_liste', ['idList' => $no]);
		$urlExpirerListe = $app->urlFor('expirer_liste', ["id" => $no]);
		$urlModifListe = $app->urlFor('modifier_Liste', ['no' => $no]);
		$urlSupprimerListe = $app->urlFor('supprimer_liste', ['no' => $no]);
		
		if($liste->estPublique()){
			$res .= <<<END
			<div class="row">
				<div class="col">
					<label class="badge badge-dark">Cette liste est publique</label>
				</div>
			</div>
END;
		}
		else{
			$urlPartage = $app->request->getHost().$app->urlFor('afficher_liste', ['no'=> $no]);
			$urlPartage.= "?token=".$liste->token;
			$urlPublication = $app->urlFor('publier_liste_vers_publique', ['no'=>$no]);
			$res .= <<<END
		
		<div class="row">
			<div class="col">	
				<form id="formToken" name="publierListe" method="post">
					<div class="btn-group all-width">
						<textarea id="token" class="form-control" rows="1" >$urlPartage</textarea>
						<button type="button" class="btn btn-success" onclick="copy()">Copier le lien</button>
						<button class="btn btn-warning" formaction=$urlPublication>Publier la liste</button>
					</div>
				</form>
			</div>
		</div>
END;
		}
		$res.=<<<END
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8">
				<div class="btn-group all-width">
END;
		if(!$liste->estExpiree()){
			$res.=<<<END
					<a class="btn btn-outline-success form-control" href="$urlAjoutItem"><b>Ajouter un Item</b></a>
					<a class="btn btn-outline-warning form-control" href="$urlExpirerListe"><b>Faire expirer la liste</b></a>
					<a class="btn btn-outline-primary form-control" href="$urlModifListe"><b>Modifier</b></a>
END;
		}
		$res.=<<<END
					<a class="btn btn-outline-danger form-control" href="$urlSupprimerListe"><b>Supprimer</b></a>
				</div>
			</div>
		</div>
	</div>
END;
		//on affiche les réservations si la liste est expirée
		if(strtotime($liste->expiration) < strtotime(date('Y-m-d'))){//liste expirée
			$afficherReservation = 2;
		}
		else{
			$afficherReservation = 0;
		}
		$res.= $this->blocItems($objets, $afficherReservation);

		$urlRetour = \Slim\Slim::getInstance()->urlFor('afficher_listes_persos');
		$res.= $this->boutonRetour($urlRetour, "Retour à vos listes de souhait");
		return $res;
	}

	/**
	*	Méthode générant le code html pour l'affichage en détail d'un item faisant partie d'une liste appartenant à l'utilisateur
	*	L'utilisateur peut ouvrir une cagnote pour l'item si ce n'est pas déjà fait, modifier ou supprimer l'item
	*	Il peut aussi associer un image à l'item soit par fichier soit par url, et retirer l'image associée
	*
	*	@domain public
	*/
	private function afficherItem(){ // Case 1

		$item = $this->param;
		
		$app = \Slim\Slim::getInstance();
		
		//Si l'item n'est pas réservé et que la liste à laquelle il appartient n'est pas expirée.
		if(!($item->estReserve() or $item->list()->first()->estExpiree())){
			$urlModifItem = $app->urlFor('modifier_Item', ['id'=>$item->id]);
			$buttonModif = <<<END
			<button class="btn btn-primary form-control" type="submit" formaction="$urlModifItem">Modifier item</button>
END;
			$urlSupprimer = $app->urlFor('supprimer_item_liste', ['id'=>$item->id]);
			$buttonSupprimer = <<<END
			<button class="btn btn-danger form-control" type="submit" formaction="$urlSupprimer">Supprimer item</button>
END;
			$urlAjouterImageOnline = $app->urlFor('modifier_image_item_online', ['id'=>$item->id]);
			$buttonAjouterImageOnline = <<<END
			<button class="btn btn-success form-control" type="submit" formaction="$urlAjouterImageOnline">Associer une image à l'item depuis une URL</button>
END;
			$urlAjouterImageInterne = $app->urlFor('modifier_image_item_interne', ['id'=>$item->id]);
			$buttonAjouterImageInterne = <<<END
			<button class="btn btn-secondary form-control" type="submit" formaction="$urlAjouterImageInterne">Associer une image à l'item à partir d'un fichier</button>
END;
			$urlSupprimerImage = $app->urlFor('supprimer_image_item', ['id'=>$item->id]);
			$buttonSupprimerImage = <<<END
			<button class="btn btn-danger form-control" type="submit" formaction="$urlSupprimerImage">Supprimer l'image associée à l'item</button>
END;
		}
		else{ //il (ou elle) l'est donc on affiche pas la possibilité de reservé
			$buttonSupprimer = "";
			$buttonModif = "";	
			$buttonAjouterImageOnline = "";
			$buttonAjouterImageInterne = "";
			$buttonSupprimerImage = "";
		}
		//Gestion des images
		$img = $item->img;
		if( filter_var( $img, FILTER_VALIDATE_URL)){
			$rootIMG = $img;
		}
		else{
			$rootIMG = \Slim\Slim::getInstance()->request()->getRootUri();
			$rootIMG .= "/img/".$img;
		}

		$texteReservation = "";
		if($item->estReserve()){
			if ($item->list()->first()->estExpiree()) {
				$texteReservation = "<h4>".$item->texteReservation(2)."</h4>";
			}
			else {
				$texteReservation = "<h4>L'item est reservé.</h4>";
			}
		}
		else{
			if($item->cagnotte == 0 and !$item->list()->first()->estExpiree()){

				$urlCagnotte = $app->urlFor('ouvrir_cagnotte', ['id'=>$item->id]);
				$texteReservation = <<<END
		<form method="post" style="margin-top:20px;">
				<button class="btn btn-secondary" formaction="$urlCagnotte">Ouvrir une cagnotte pour cet item</button>
		</form>
END;
			}
		}
		$rootListes = \Slim\Slim::getInstance()->urlFor('afficher_liste', ['no'=>$item->liste_id]);
		$boutonRetour = $this->boutonRetour($rootListes, "Retour à la liste");

		$res = <<<END
			<div class="container">
				<div class="row centered">
					<div class="col">
						<h1><span class="badge badge-primary">$item->nom</span></h1>
						<p>$item->descr</p>
						<p>Valeur : $item->tarif €</p>
						<img alt="$img" src="$rootIMG">
						$texteReservation
					</div>
				</div>

				<div class="row centered">
					<div class="col centered">
						<form id="ModifierUnItem" method="get">
							<div class="btn-group form-group">
								$buttonSupprimer
								$buttonModif	
							</div>
							<div class="btn-group form-group">
								$buttonAjouterImageOnline
								$buttonAjouterImageInterne
								$buttonSupprimerImage
							</div>
						</form>
					</div>
				</div>

				$boutonRetour

			</div>
END;
		return $res;
	}

	
	
	/**
	*	Méthode générant le texte html de la page pour créer une liste
	*	L'utilisateur doit indiquer le nom de la liste, la description, et la date d'expiration
	*
	*	@domain public
	*/
	private function afficherCreationListe(){
		$url = \Slim\Slim::getInstance()->urlFor('enregistrer_liste');
		$date = date('Y-m-d');
		$date_min = date('Y-m-d', strtotime($date.' + 1 DAY'));
		$res = <<<END

	<div class="container">
		<div class="row centered">
			<div class="col">
				<form id="createlist" method="post">
					<div class="form-group">
						<label>Nom de la liste</label>
						<input class="form-control" type="text" name="titre">
					</div>
					<div class="form-group">
						<label>Description</label>
						<input class="form-control" type="text" name="description">
					</div>
					<div class="form-group">
						<label>Date d'expiration</label>
						<input class="form-control" type="date" min="$date_min" name="expiration">
					</div>
					<button class="btn btn-primary" type="submit" formaction="$url"name="creer_liste" value="valid_f1">valider</button>
				</form>
			</div>
		</div>
	</div>
END;
		$url = \Slim\Slim::getInstance()->urlFor('route_defaut');
		$res .= $this->boutonRetour($url, "Retour à la page d'accueil");
		return $res;
	}
	
	/**
	*	Méthode générant le code html lorsque l'utilisateur essaye d'acceder à une page necessitant d'être connecté
	*
	*	@domain public
	*/
	public function pasConnecte(){
		$res = "<p>Désolé vous devez être connécté pour avoir acces à cette zone</p>";
		$url = \Slim\Slim::getInstance()->urlFor('route_defaut');
		$res .= $this->boutonRetour($url, "Retour à la page d'accueil");
		return $res;
	}
	
	/**
	*	Méthode générant le texte html de la page pour modifier une liste
	*	L'utilisateur doit indiquer le nouveau nom de la liste, la nouvelle description, et la nouvelle date d'expiration
	*
	*	@domain public
	*/
	private function afficherModificationListe(){
		
		$url = \Slim\Slim::getInstance()->urlFor('modifier_Liste', ['no'=>$this->param]);
		$date = date('Y-m-d');
		$date_min = date('Y-m-d', strtotime($date.' + 1 DAY'));
		$res = <<<END
		
		
		<div class="container">
			<div class="row">
				<div class="col">
					<h1>Modification de la liste</h1>
					<form id="modifierlist" method="post" action=$url>
							<div class="form-group">
								<label>Nouveau nom de la liste</label>
								<input class="form-control" type="text" name="new_titre" required>
							</div>
							<div class="form-group">
								<label>Nouvelle description</label>
								<input class="form-control" type="text" name="new_description" required>
							</div>
							<div class="form-group">
								<label>Nouvelle date d'expiration</label>
								<input class="form-control" type="date" min="$date_min" name="new_expiration" required>
							</div>
							<div class="centered">
								<button class="btn btn-primary" type="submit" name="valider_modif" value="valider">valider</button>
							</div>
					</form>
				</div>
			</div>
END;
		$url = \Slim\Slim::getInstance()->urlFor('afficher_liste', ['no'=>$this->param]);
		$res .= $this->boutonRetour($url, "Retour à la liste");
		$res .="</div>";
		return $res;
	}
	
	/**
	*	Méthode générant le code html de la page pour ajouter un item à la liste appartenant à l'utilisateur
	*	L'utilisateur doit indiquer le nom de l'item, la description de l'item et le prix	
	*
	*	@domain public
	*/
	private function afficherAjouterItemListe(){
		
		$url = \Slim\Slim::getInstance()->urlFor('ajouter_item_a_liste', ['idList'=>$this->param]);
		
		$res = <<<END
	<div class="container">
		<div class="row">
			<div class="col-3"></div>
			<div class="col-6">
				<form id="ajoutItemList" method="post" action=$url>
					<div class="form-group">
						<label for="nomItem">Nom de l'Item</label>
						<input id="nomItem" class="form-control" type="text" name="nomItem" required>
					</div>
					<div class="form-group">
						<label for="descItem">Description de l'Item</label>
						<input id="descItem" class="form-control" type="text" name="descriptionItem" required>
					</div>
					<div class="form-group">
						<label for="prixItem">Prix de l'item</label>
						<input id="prixItem" class="form-control" type="number" name="tarifItem" required>
					</div>
					<div class="centered">
						<button class="btn btn-primary" type="submit" name="valider_Ajout_Item" value="valider">Valider</button>
					</div>
				</form>
			</div>
			<div class="3"></div>
		</div>
END;
		$url = \Slim\Slim::getInstance()->urlFor('afficher_liste', ['no'=>$this->param]);
		$res .= $this->boutonRetour($url, "Retour à la liste");
		$res .= "</div>";
		return $res;	
	}
	
	/**
	*	Méthode générant le code html de la page pour modifer un item à la liste appartenant à l'utilisateur
	*	L'utilisateur doit indiquer le nouveau nom de l'item, la nouvelle description de l'item et le nouveau prix
	*
	*	@domain public	
	*/
	private function afficherModificationItem(){
		
		$url = \Slim\Slim::getInstance()->urlFor('modifier_Item', ['id' => $this->param]);
		
		$urlRet = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id'=>$this->param]);
		$boutonRetour = $this->boutonRetour($urlRet, "Retour à l'item");

		$res = <<<END
	<div class="container">
		<div class="row">
			<div class="col">
				<form id="ModifItemList" method="post" action=$url>
					<div class="form-group">
						<label for="nomItem">Nouveau nom de l'Item</label>
						<input id="nomItem" class="form-control" type="text" name="new_nomItem" required>
					</div>
					<div class="form-group">
						<label for="descItem">Nouvelle description de l'Item</label>
						<input id="descItem" class="form-control" type="text" name="new_descriptionItem" required>
					</div>
					<div class="form-group">
						<label for="prixItem">Nouveau prix de l'item</label>
						<input id="prixItem" class="form-control" type="number" name="new_tarifItem" required>
					</div>
					<div class="centered">
						<button class="btn btn-primary" type="submit" name="valider_modif_Item" value="valider">Valider</button>
					</div>
				</form>
			</div>
		</div>
		$boutonRetour
	</div>			
END;
		
		return $res;
	
	}
	
	//Ecran de changement d'une image par url
	/**
	*	Méthode générant le code html de la page pour associer une image à un item
	*	grâce à un url
	*
	*	@domain public
	*/
	private function affichageImageItemOnline(){

		$res = <<<END
		<div class="container centered">
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
					<form id="ModifImageItemOnline" method="post">
						<div class="form-group">
							<label for="nomImage">URL de l'image que vous voulez utiliser</label>
							<input id="nomImage" class="form-control" type="text" name="Url_Image_Item_Online" required>
						</div>
						<button class="btn btn-primary" type="submit" name="valider_image_item_online" value="valider">Valider</button>
					</form>
				</div>
				<div class="col-3"></div>
			</div>
		</div>
END;
		$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id'=>$this->param]);
		$res .= $this->boutonRetour($url, "Retour à l'item");
		return $res;
	}
	
	//Ecran de changement d'image par un upload de fichier
	/**
	*	Méthode générant le code html de la page pour associer une image à un item
	*	depuis un fichier
	*
	*	@domain public
	*/
	private function affichageImageItemInterne(){	
		$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id'=>$this->param]);
		$boutonRetour = $this->boutonRetour($url, "Retour à l'item");
		$res = <<<END
		<div class="container">
			<div class="row">
				<div class="col-4"></div>
				<div class="col-4">
					<form id="uploaderImage" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="MAX_FILE_SIZE" value="10000000000">
						<div class="form-group">
							<input type="file" name="new_Image" accept="image/jpeg">
						</div>
						<div class="form-group">
							<button class="btn btn-primary form-control" type="submit" name="uploaderImage_validation">EnvoyerLeFichier</button>
						</div>
					</form>
				</div>
				<div class="col-4"></div>
			</div>
			$boutonRetour
		</div>
END;
		return $res;
	}

	/**
	*	Méthode générant le texte html de la page de modification du compte
	*	l'utilisateur doit indiquer nom, prénom et son mot de passe actuel
	*	Il peut aussi acceder à la page de changement de mot de passe ou supprimer mon compte
	*
	*	@domain public
	*/
	public function afficherPageModificationCompte(){
		$msg = "";		
		if(isset($_COOKIE['erreur_modif'])){
			$msg = $_COOKIE['erreur_modif'];
		}

		$user = $this->param;
		$prenom = explode(" ", $user->nom)[0];
		$nom = explode(" ", $user->nom)[1];

		$urlRet = \Slim\Slim::getInstance()->urlFor('route_defaut');
		$boutonRetour = $this->boutonRetour($urlRet, "Retour à la page d'accueil");
		
		$urlChangerMdp = \Slim\Slim::getInstance()->urlFor('changer_mdp');
		$urlSupprCompte = \Slim\Slim::getInstance()->urlFor('supprimer_compte');
	
		$res =  <<<END
		<div class="container">
			<div class="row">
				<div class="col-2"></div>
				<div class="col-8">
					<form id="inscription" method="post">
						$msg
						<div class="form-group">
							<label class="form-label">Nom</label>
							<input class="form-control" value="$nom" type="text" name="nom" required>
						</div>
						<div class="form-group">
							<label>Prenom</label>
							<input class="form-control" value="$prenom" type="text" name="prenom" required>
						</div>
						<div class="form-group">
							<label>Confirmez votre mot de passe</label>
							<input class="form-control" type="password" name="mdp" required>
						</div>
						<button class="btn btn-primary" type="submit" name="valider_connexion">Enregistrer les modifications</button>
					</form>
				</div>
				<div class="col-2"></div>

			</div>
			<div class="row">
				<div class="col-4">
					<div class="btn-group">
						<a class="btn btn-warning" href="$urlChangerMdp">Changer de mot de passe</a>
						<a type="button" class="btn btn-danger" href="$urlSupprCompte">Supprimer mon compte</a>
					</div>
				</div>
				<div class="col-8"></div>
			</div>
			$boutonRetour
		</div>
END;
		
		return $res;

	}

	/**
	*	Méthode générant le texte html de la page de changement de mot de passe
	*	L'utilisateur doit indiquer son ancien mot de passe et son nouveau mot de passe
	*
	*	@domain public
	*/
	public function afficherPageModificationMDP(){
		$msg = "";		
		if(isset($_COOKIE['erreur_modif'])){
			$msg = $_COOKIE['erreur_modif'];
		}
		$urlRet = \Slim\Slim::getInstance()->urlFor('modification_compte');
		$boutonRetour = $this->boutonRetour($urlRet, "Retour à la modification du compte");
		return <<<END
		<div class="container">
			$msg
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6 centered">
					<form method="post">
						<div class="form-group">
							<label for="oldmdp">Ancien mot de passe</label>
							<input class="form-control" type="password" id="oldmdp" name="old_password">
						</div>
						<div class="form-group">
							<label for="newmdp">Nouveau mot de passe</label>
							<input class="form-control" type="password" id="newmdp" name="new_password">
						</div>
						<button class="btn btn-primary" type="submit">Valider</button>
					</form>
				</div>
				<div class="col-3"></div>
			</div>
			$boutonRetour
		</div>
END;
	}
	

}