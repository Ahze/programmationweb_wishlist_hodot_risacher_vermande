function copy() {
	var copyText = document.getElementById("token");
	copyText.select();
	document.execCommand("copy");
	alert("Copié : " + copyText.value);
} 

function toggle(id){
	if(document.getElementById(id).style.display == "none"){
		document.getElementById(id).style.display = "inline-block";
	}
	else{
		document.getElementById(id).style.display = "none";
	}
}