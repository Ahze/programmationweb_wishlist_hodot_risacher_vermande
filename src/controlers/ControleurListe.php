<?php

namespace mywishlist\controlers;

use \mywishlist\models\Liste;
use \mywishlist\models\Message;
use \mywishlist\vue\VueParticipant;
use \mywishlist\vue\VueCreateur;

/**
 * Classe controleur des listes.
 *
 * Classe controleur des listes,
 * permettant de traiter les differentes fonctionnalites liees aux listes d'items
 * et de rediriger vers les vues correspondantes.
 *
 * Classe controleur des listes,
 * permettant de traiter les differentes fonctionnalites liees aux listes
 * et de rediriger vers les vues correspondantes.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class ControleurListe {
	
	/**
	 * Methode pour afficher une liste.
	 *
	 * Methode pour afficher une liste et ses items.
	 *
	 * Methode pour afficher une liste et ses items.
	 * Chaque item est associe a son nom, son image
	 * et sa reservation si la liste est expiree pour le proprietaire
	 * (ou si ce n'est pas le proprietaire, mais les visiteurs ne verront pas le message de reservation).
	 *
	 * @param int[$no] Numero de la liste dans la base de donnees
	 * @domain public
	 */
	public function afficherListe($no){
		$app = \Slim\Slim::getInstance();
		$list = \mywishlist\models\Liste::find($no);
		if(is_null($list)){
			$app->redirect($app->urlFor('erreur'));
		}

		$proprietaire = false;
		if(isset($_SESSION['profil']['user_id'])){
			if($_SESSION['profil']['user_id'] == $list->user_id){
				$proprietaire = true;
			}
		}
		$token_valide = $app->request->get('token') === $list->token;

		$tab[0] = $list;
		$tab[1] = array();
		foreach ($list->items()->get() as $obj) {
			$tab[1][] = $obj;
		}

		if($proprietaire){	
			$vue = new VueCreateur($tab);
			$vue->render(VueCreateur::PageContenuListe);
		}
		elseif($list->estPublique() or $token_valide and !$list->estExpiree()){
			$vue = new VueParticipant($tab);
			$vue->render(VueParticipant::PageContenuListe);
		} 
		else{//erreur
			$app->redirect($app->urlFor('erreur'));
		}
	}
	
	/**
	 * Methode pour afficher toutes les listes.
	 *
	 * Methode permettant l'affichage des listes publiques et appartenants a la personne connectee.
	 *
	 * Methode permettant l'affichage des listes publiques et de celles appartenants a la personne connectee.
	 * Cette methode n'affiche pas directement mais filtre les listes et renvoit vers la methode d'affichage correspondante.
	 *
	 * @domain public
	 */
	public function afficherListes(){
		if(isset($_SESSION['profil']['user_id'])){
			$listesCreateur = Liste::where('user_id', '=', $_SESSION['profil']['user_id'])->get();
			$vue = new VueCreateur($listesCreateur);
			$vue->render(VueCreateur::PageListes);
		}
		else{
			$listesPubliques = \mywishlist\models\Liste::whereNull('token')->get();
			$listes = array();
			foreach ($listesPubliques as $list) {
				//on vérifie qu'elles soit encore valide ^^
				if(!$list->estExpiree() && $list->estPublique()){
					$listes[] = $list;
				}
			}
			$vue = new VueParticipant($listes);
			$vue->render(VueParticipant::PageListes);
		}
	}	
	/**
	 * Methode permettant d'afficher les listes publiques.
	 *
	 * Methode qui filtre les listes publiques et les affiches.
	 *
	 * Methode qui filtre les listes publiques et les affiches.
	 *
	 * @domain public
	 */
	public function afficherListesPubliques(){
		$listesPubliques = \mywishlist\models\Liste::whereNull('token');
		if(isset($_SESSION['profil'])){
			$listesPubliques = $listesPubliques->where('user_id', '!=', $_SESSION['profil']['user_id']);
		}
		$listesPubliques = $listesPubliques->get();
		$listes = array();
		foreach ($listesPubliques as $list) {
			if(!$list->estExpiree() and $list->estPublique()){
				$listes[] = $list;
			}
		}
		$vue = new VueParticipant($listes);
		$vue->render(VueParticipant::PageListes);
	}
	
	/**
	 * Methode affichant les listes personnelles.
	 *
	 * Methode affichant les listes appartenant au compte connecte.
	 *
	 * Methode affichant les listes appartenant au compte connecte.
	 * Il est donc necessaire de se connecter pour que cette methode fonctionne.
	 *
	 * @domain public
	 */
	public function afficherListesPersos(){
		$listesCreateur = Liste::where('user_id', '=', $_SESSION['profil']['user_id'])->get();
		$vue = new VueCreateur($listesCreateur);
		$vue->render(VueCreateur::PageListes);	
	}
	
	/**
	 * Methode affichant la page de modification de liste.
	 *
	 * Methode qui affiche la page de modification d'une liste.
	 *
	 * Methode qui affiche la page de modification d'une liste.
	 * Pour etre utilisee, il faut que le compte connecte soit celui du titulaire de la liste.
	 * Il est possible d'y modifier le nom, la description et la date d'expiration de la liste.
	 *
	 * @param int[$no] ID de la liste dans la base de donnee que l'on souhaite modifier.
	 * @domain public
	 */
	public function afficherModificationListe($no){
		$v = new VueCreateur($no);
		$v->render(VueCreateur::PageModifierListe);
	}
	
	/**
	 * Methode modifiant une liste.
	 *
	 * Methode effectuant une modification sur une liste.
	 *
	 * Methode effectuant une modification sur une liste.
	 * Le compte connecte doit etre celui du titulaire de la liste pour que cette methode fonctionne.
	 * Doivent etre presents dans le $_POST :
	 * string 'new_titre'
	 * string 'new_description'
	 * string 'new_expiration'
	 *
	 * @param int[$no] ID de la liste dans la base de donnees que l'on souhaite modifier.
	 * @domain public
	 */
	public function ModifierListe($no){
		$app = \Slim\Slim::getInstance();
		$liste = \mywishlist\models\Liste::find($no); 
		$liste->titre = $app->request()->post('new_titre');
		$liste->description = $app->request()->post('new_description');
		$liste->expiration = $app->request()->post('new_expiration');
		//on verifie que ne modifie pas la liste vers une expiration
		if($liste->estExpiree()){

		}
		else{
			$liste->save();
		}
	}
	
	/**
	 * Methode faisant expirer une liste.
	 *
	 * Methode permettant de faire expirer immediattement une liste.
	 *
	 * Methode employable pour faire expirer immediatemment une liste.
	 * La liste est expiree en la mettant a la date courante du serveur.
	 *
	 * @param int[$id] ID de la liste dans la base de donnees a faire expirer.
	 * @domain public
	 */
	public function ExpirerListe($id){
		$app = \Slim\Slim::getInstance();
		$liste = \mywishlist\models\Liste::find($id);
		$liste->expiration = date('Y-m-d');
		$liste->save();
		$token = "";
		if(!is_null($app->request->get('token'))){
			$token = "?token=".$app->request->get('token');
		}
		$app->redirect($app->urlFor('afficher_liste', ['no'=>$id]).$token);
	}

	/**
	 * Methode permettant de rendre une liste publique.
	 *
	 * Methode permettant de rendre une liste publique.
	 * Seul le titulaire de la liste peut effectuer cette action.
	 *
	 * Methode permettant de rendre une liste publique.
	 * Seul le titulaire de la liste peut effectuer cette action.
	 * Rendre une liste publique est irreversible.
	 * Une liste publique sera visible par tous les visiteurs du site, meme les non-inscrits.
	 *
	 * @param int[$no] ID de la liste dans la base de donnees a rendre publique.
	 * @domain public
	 */
	public function publierListe($no){
		$l = Liste::find($no);
		$l->token = null;
		$l->save();
	}
	
	/**
	 * Methode affichant la page de creation de liste.
	 *
	 * Methode permettant d'afficher la page ou l'on cree une liste.
	 * Il est necessaire d'etre connecter pour appeller cette methode.
	 *
	 * Methode permettant d'afficher la page ou l'on cree une liste.
	 * Il est necessaire d'etre connecter pour appeller cette methode.
	 *
	 * @domain public
	 */
	public function afficherCreationListe(){
		(new VueCreateur(NULL))->render(VueCreateur::PageCreerListe);
	}
	
	/**
	 * Methode pour enregistrer la creation d'une nouvelle liste.
	 *
	 * Methode pour enregistrer la creation d'une nouvelle liste.
	 * Il est necessaire d'etre connecter pour appeller cette methode.
	 *
	 * Methode pour enregistrer la creation d'une nouvelle liste.
	 * Il est necessaire d'etre connecter pour appeller cette methode.
	 * Dans le $_POST doivent etre saisis :
	 * string titre
	 * string description
	 * date expiration
	 *
	 * @domain public
	 */
	public function enregistrerListe(){
		$l = new Liste();
		$app = \Slim\Slim::getInstance();
		$l->user_id = $_SESSION['profil']['user_id'];
		$l->titre = $app->request()->post('titre');
		$l->description = $app->request()->post('description');
		$l->expiration = $app->request()->post('expiration');
		$l->token = $this->genererToken();
		if($l->estExpiree()){

		}
		else{
			$l->save();
		}

		return $l;
	}
	
	/**
	 * Methode permettant de supprimer une liste.
	 *
	 * Methode permettant la suppression d'une liste et de tous ses items.
	 * Cette action est definitive.
	 *
	 * Methode permettant la suppression d'une liste et de tous ses items.
	 * Cette action est definitive.
	 *
	 * @param int[$id] ID de la liste dans la base de donnees a supprimer.
	 */
	public function supprimerListe($no){
		$liste = Liste::find($no);
		if(!is_null($liste)){
			$liste->supprimer();
		}
		$this->afficherListes();
	}

	/**
	 * Methode permettant de generer un token pour la liste.
	 *
	 * Methode permettant de generer un token pour la liste.
	 * Un token permet a des personnes non proprietaires de la liste de pouvoir y acceder.
	 *
	 * Methode permettant de generer un token pour la liste.
	 * Le token cree est lie a une URL qui permet a quelqu'un d'autre que le proprietaire de la liste
	 * d'acceder a cette derniere sans qu'elle soit publique.
	 * Les tokens ont une duree de vie limitee.
	 *
	 * @domain public
	 */
	private function genererToken() {
		$length = 10;
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
	/**
	 * Methode permettant l'ajout d'un message a une liste.
	 *
	 * Methode ajoutant un message, tel un commantaire, a la liste et le rendant public a tous.
	 *
	 * Methode ajoutant un message, tel un commantaire, a la liste et le rendant public a tous.
	 * Ce message une fois cree ne peut etre supprime.
	 * Il faut etre connecter pour laisser une message.
	 * dans le $_POST doit etre presente la variable string 'inputMessage' (le commentaire a laisser).
	 *
	 * @param int[$id] ID de la liste dans la base de donnees dans laquelle laisser le message.
	 * @domain public
	 */
	public function ajouterMessage($id){
		$app = \Slim\Slim::getInstance();
		if( ! is_null($app->request->post('inputMessage')) ){
			$message = new Message();
			$message->liste_id = $id;
			$message->texte = $app->request->post('inputMessage');
			$message->save();
		}
		$token = "";
		if(!is_null($app->request->get('token'))){
			$token = "?token=".$app->request->get('token');
		}
		$app->redirect($app->urlFor('afficher_liste', ['no'=>$id]).$token);
	}
}