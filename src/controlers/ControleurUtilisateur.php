<?php

namespace mywishlist\controlers;

use mywishlist\models\Liste;
use \mywishlist\vue\VueParticipant;
use \mywishlist\vue\VueCreateur;
use \mywishlist\models\Utilisateur as Utilisateur;
use \mywishlist\models\Authentification as Auth;

/**
 * Classe controleur des comptes d'utilisateurs.
 *
 * Classe controleur des comptes d'utilisateurs,
 * permettant de traiter les differentes fonctionnalites liees aux comptes d'utilisateurs
 * et de rediriger vers les vues correspondantes.
 *
 * Classe controleur des comptes d'utilisateurs,
 * permettant de traiter les differentes fonctionnalites liees aux comptes d'utilisateurs
 * et de rediriger vers les vues correspondantes.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class ControleurUtilisateur {

	/**
	 * Methode permettant l'affichage de la page de connexion.
	 *
	 * Methode permettant l'affichage de la page de connexion.
	 *
	 * Methode permettant l'affichage de la page de connexion.
	 *
	 * @domain public
	 */
	public function afficherPageConnexion(){
		$v = new VueParticipant(null);
		$v->render(VueParticipant::PageConnexion);
	}

	/**
	 * Methode essayant de connecter un utilisateur avec les informations saisies.
	 *
	 * Methode essayant de connecter un utilisateur avec les informations saisies.
	 * Si la connexion echoue, l'utilisateur est renvoye sur la page de connexion.
	 * Sinon, il est envoye vers la premiere page du site avec le statut connecte.
	 *
	 * @param string[$email] Adresse mail avec laquelle tente de se connecter l'utilisateur.
	 * @param string[$mdp] Mot de passe avec lequel tente de se connecter l'utilisateur.
	 * @domain public
	 */
	public function essayerConnexion($email, $mdp){
		$app = \Slim\Slim::getInstance();
		if(Auth::authentifier($email, $mdp)){
			$app->redirect($app->urlFor('route_defaut'));
		}
		else{
			$app->redirect($app->urlFor('connexion'));
		}
	}

	/**
	 * Methode affichant la page d'inscription d'un compte.
	 *
	 * Methode affichant la page qui permet a l'utilisateur de s'enregistrer sur le site.
	 *
	 * Methode affichant la page qui permet a l'utilisateur de s'enregistrer sur le site.
	 * Une fois l'utilisateur enregistre, il disposera d'un compte dans la base de donnees.
	 *
	 * @domain public
	 */
	public function afficherPageInscription(){
		$v = new VueParticipant(null);
		$v->render(VueParticipant::PageInscription);
	}

	/**
	 * Methode tentant d'inscrire quelqu'un sur le site.
	 *
	 * Methode tentant d'inscrire quelqu'un sur le site.
	 *
	 * Methode tentant d'inscrire quelqu'un sur le site.
	 * Si l'inscription echoue (adresse non valide, compte existant...)
	 * l'utilisateur est renvoye vers la page d'inscription.
	 * Sinon, il est renvoye vers la premiere page du site avec le statut connecte.
	 *
	 * @param string[$nom] Nom de l'utilisateur.
	 * @param string[$prenom] Prenom de l'utilisateur.
	 * @param string[$email] Adresse email de l'utilisateur.
	 * @param string[$mdp] Mot de passe de l'utilisateur.
	 */
	public function envoyerMailInscription($nom, $prenom, $email, $mdp){
    	
		$app = \Slim\Slim::getInstance();
		if(Auth::creerUtilisateur("$prenom $nom", $email, $mdp)){
			$app->redirect($app->urlFor('route_defaut'));
		}
		else{
			$app->redirect($app->urlFor('inscription'));
		}
	}

	/**
	 * Methode permettant d'afficher la modification de compte.
	 *
	 * Methode permettant d'afficher la page de modification de compte.
	 *
	 * Methode permettant d'afficher la page de modification de compte.
	 * Il est necessaire d'etre connecte pour modifier son compte.
	 * Dans cette page ne peut etre modifie que le nom de l'utilisateur.
	 *
	 * @domain public
	 */
	public function afficherModificationCompte(){
		$u = Utilisateur::find($_SESSION['profil']['user_id']);
		$v = new VueCreateur($u);
		$v->render(VueCreateur::PageModifierCompte);
	}

	/**
	 * Methode permettant de modifier un compte.
	 *
	 * Methode permettant de modifier un compte.
	 * Il est necessaire d'etre connecter pour appliquer cette methode.
	 *
	 * Methode permettant de modifier un compte.
	 * Il est necessaire d'etre connecter pour appliquer cette methode.
	 * Dans le $_POST :
	 * string prenom
	 * string nom
	 * string mdp (ce dernier n'est pas modifie, mais sert a verifier l'identite de l'utilisateur)
	 *
	 * @domain public
	 */
	public function modifierCompte(){
		$app = \Slim\slim::getInstance();
		if(empty($_POST)){
			$app->redirect($app->urlFor('erreur'));
		}
		$u = Utilisateur::find($_SESSION['profil']['user_id']);
		if(!password_verify($app->request->post('mdp'), $u->mdp)){
			setcookie('erreur_modif', 'Le mot de passe ne correspond pas', time()+60);
			$app->redirect($app->urlFor('modification_compte'));
		}
		$u->nom = $app->request->post('prenom')." ".$app->request->post('nom');
		$u->save();
		$app->redirect($app->urlFor('route_defaut'));
	}

	/**
	 * Methode affichant la page de modification de mot de passe.
	 *
	 * Methode affichant la page de modification de mot de passe.
	 * Il faut etre connecter pour afficher cette page.
	 *
	 * Methode affichant la page de modification de mot de passe.
	 * Il faut etre connecter pour afficher cette page.
	 *
	 * @domain public
	 */
	public function afficherModificationMotDePasse(){
		$v = new VueCreateur(null);
		$v->render(VueCreateur::PageModifierMDP);
	}

	/**
	 * Methode pour modifier le mot de passe d'un compte.
	 *
	 * Methode pour modifier le mot de passe d'un compte.
	 * Il est imperatif d'etre connecter pour appeler cette methode.
	 *
	 * Methode pour modifier le mot de passe d'un compte.
	 * Il est imperatif d'etre connecter pour appeler cette methode.
	 * Dans le $_POST doivent etre present
	 * old_password (pour verifier que c'est bien l'utilisateur qui tente de modifier son mot de passe)
	 * new_password
	 *
	 * @domain public
	 */
	public function modifierMotDePasse(){
		$app = \Slim\slim::getInstance();
		if(empty($_POST)){
			$app->redirect($app->urlFor('erreur'));
		}
		$u = Utilisateur::find($_SESSION['profil']['user_id']);
		if(password_verify($app->request->post('old_password'), $u->mdp)){
			$u->mdp = password_hash($app->request->post('new_password'), PASSWORD_DEFAULT, ['cost'=>12]);
			$u->save();
			Auth::deconnexion();
			$app->redirect($app->urlFor('route_defaut'));
		}
		else{
			setcookie('erreur_modif', 'Votre ancien mot de passe est incorrect', time()+60);
			//$app->redirect($app->urlFor('changer_mdp'));
			var_dump($_POST);
		}
	}

	/**
	 * Methode pour supprimer un compte.
	 *
	 * Methode supprimant un compte, mais ne supprime pas les listes qu'il a cree.
	 *
	 * Methode supprimant un compte, mais ne supprime pas les listes qu'il a cree.
	 *
	 * @domain public
	 */
	public function supprimerCompte(){
		$app = \Slim\slim::getInstance();
		Utilisateur::find($_SESSION['profil']['user_id'])->supprimer();
		$app->redirect($app->urlFor('route_defaut'));
	}
	

}	