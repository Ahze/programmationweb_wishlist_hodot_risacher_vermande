<?php

namespace mywishlist\controlers;

use \mywishlist\vue\VueParticipant;
use \mywishlist\vue\VueCreateur;
use \mywishlist\vue\Vue;
use \mywishlist\models\Item;
use \mywishlist\models\Liste;
use mywishlist\controlers\ControleurListe;

/**
 * Classe controleur des items.
 *
 * Classe controleur des items,
 * permettant de traiter les differentes fonctionnalites liees aux items
 * et de rediriger vers les vues correspondantes.
 *
 * Classe controleur des items,
 * permettant de traiter les differentes fonctionnalites liees aux items
 * et de rediriger vers les vues correspondantes.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class ControleurItem {
	
	/**
	 * Methode permettant d'afficher la page d'un item.
	 *
	 * Methode permettant d'afficher la page d'un item.
	 * Sur cette page est presente le nom, la description et l'image d'un item,
	 * ainsi que des options variants selon la personne qui regarde la page.
	 *
	 * Methode permettant d'afficher la page d'un item.
	 * Sur cette page est presente le nom, la description et l'image d'un item,
	 * ainsi que des options variants selon la personne qui regarde la page.
	 *
	 * Le createur de l'item peut modifier l'item (nom, description, image)
	 * et ouvrir un cagnotte dessus.
	 *
	 * Les autres peuvent reserver l'item ou participer a la cagnotte et peuvent ajouter un mot a leur reservation.
	 * Les personnes non-connectees auront egalement la possibilite d'entrer leur nom pour reserver l'item.
	 *
	 * @param int[$id] ID de l'item dans la base de donnee qu'il faut afficher.
	 * @domain public
	 */
	public function afficherItem($id){
		$app = \Slim\Slim::getInstance();
		$item = Item::find($id);
		
		if ($item === null){
			$app->redirect($app->urlFor('erreur'));
		}
		else{
			$proprietaire = false;
			$list = $item->list()->first();
			if(isset($_SESSION['profil']['user_id'])){
				if($list->user_id == $_SESSION['profil']['user_id']){
					$proprietaire = true;
				}
			}
			$token_valide = $app->request->get('token') === $list->token;

			if($proprietaire){
				$vue = new VueCreateur($item);
				$vue->render(VueCreateur::PageItem);
			}
			elseif($list->estPublique() or $token_valide){
				$vue = new VueParticipant($item);
				$vue->render(VueParticipant::PageItem);
			}	 
			else{
				$vue = new VueParticipant(null);
				$vue->render(VueParticipant::ContenuInaccessible);			
			}
		}
	}
	
	/**
	 * Methode pour afficher la page d'ajout d'un item a une liste.
	 *
	 * Methode permettant d'afficher la page d'ajout d'un item a une liste.
	 * Cette methode n'est utilisable que si la personne est connectee et tente de modifier une de ses listes.
	 *
	 * Methode permettant d'afficher la page d'ajout d'un item a une liste.
	 * Cette methode n'est utilisable que si la personne est connectee et tente de modifier une de ses listes.
	 * Si la personne tente d'attendre cette page alors qu'elle n'en a pas les droits, elle finit sur une page speciale dediee a cet effet.
	 *
	 * @param int[$idList] ID dans la base de donnee de la liste sur laquelle on ajoute un item.
	 * @domain public
	 */
	public function afficherAjoutItemAListe($idList){
		$app = \Slim\Slim::getInstance();
		if(isset($_POST['valider_Ajout_Item']) && $app->request()->post('valider_Ajout_Item')=='valider'){
			$this->enregisterItem($idList);
		}
		else{
			(new vueCreateur($idList))->render(VueCreateur::PageAjouterItem);
		}
	}

	/**
	 * Methode pour ajouter un item a une liste.
	 *
	 * Methode permettant d'ajouter un item a une liste.
	 * Cette methode n'est utilisable que si la personne est connectee et tente de modifier une de ses listes.
	 *
	 * Methode permettant d'ajouter un item a une liste.
	 * Cette methode n'est utilisable que si la personne est connectee et tente de modifier une de ses listes.
	 * Avec cette methode doivent etre presents dans le $_POST les valeurs
	 * 'nomItem' (string), 
	 * 'descriptionItem' (string),
	 * et 'tarifItem' (float).
	 *
	 * @param int[$idList] ID dans la base de donnee de la liste sur laquelle on ajoute un item.
	 * @domain public
	 */
	public function enregisterItem($idList){
		$item = new Item();
		$app = \Slim\Slim::getInstance();
		$item->liste_id = $idList;
		$item->nom = $app->request()->post('nomItem');
		$item->descr = $app->request()->post('descriptionItem');
		$item->tarif = $app->request()->post('tarifItem');
		$item->img = 'random.jpg';
		$item->save();
		
		//renvoit à l'item
		$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id' => $item->id]);
		header("Location: ".$url);
		exit();
	}
	
	/**
	 * Methode pour afficher la page de modification d'un item.
	 *
	 * Methode permettant d'afficher la page de modification d'un item.
	 * Seul le createur de l'item peut atteindre cette page.
	 *
	 * Methode permettant d'afficher la page de modification d'un item.
	 * Sur cette page doivent etre saisis :
	 * le nouveau nom de l'item (string),
	 * la nouvelle description de l'item (string),
	 * le nouveau tarif de l'item (float).
	 *
	 * @param int[$id] ID dans la base de donnee de l'item que l'on modifie.
	 * @domain public
	 */
	public function afficherModificationItem($id){
		$app = \Slim\Slim::getInstance();
		if(isset($_POST['valider_modif_Item']) and $app->request()->post('valider_modif_Item')=='valider'){
			$this->ModifierItem($id);
		}
		else{
			(new VueCreateur($id))->render(VueCreateur::PageModifierItem);
		}
	}
	
	/**
	 * Methode pour modifier un item.
	 *
	 * Methode permettant de modifier un item existant.
	 * Cette methode n'est utilisable que si la personne est connectee et tente de modifier un item qu'il a cree et qui n'a pas deja ete reserve.
	 *
	 * MMethode permettant de modifier un item existant.
	 * Cette methode n'est utilisable que si la personne est connectee et tente de modifier un item qu'il a cree et qui n'a pas deja ete reserve.
	 * Avec cette methode doivent etre presents dans le $_POST les valeurs
	 * 'nomItem' (string), 
	 * 'descriptionItem' (string),
	 * et 'tarifItem' (float).
	 *
	 * @param int[$id] ID dans la base de donnee de l'item que l'on modifie.
	 * @domain public
	 */
	public function ModifierItem($id){
		$app = \Slim\Slim::getInstance();
		$item = Item::find($id); 
		$item->nom = $app->request()->post('new_nomItem');
		$item->descr = $app->request()->post('new_descriptionItem');
		$item->tarif = $app->request()->post('new_tarifItem');
		$item->save();
		
		//retour à la page modifiée
		$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id' => $item->id]);
		header("Location: ".$url);
		exit();
	}
	
	/**
	 * Methode d'affichage de la page de modification d'image d'item par URL.
	 *
	 * Methode permettant d'afficher la page de modification de l'image d'un item avec une URL.
	 * Dans cette page devra etre saisie une URL vers un lien image valide.
	 *
	 * Methode permettant d'afficher la page de modification de l'image d'un item avec une URL.
	 * Dans cette page devra etre saisie une URL vers un lien image valide.
	 *
	 * @param int[$id] ID dans la base de donnees de l'item dont on veut changer l'image.
	 */
	public function AffichageModifierImageItemOnline($id){
		$app = \Slim\Slim::getInstance();
		if(isset($_POST['valider_image_item_online']) and $app->request()->post('valider_image_item_online')=='valider' ){
			$this->ModifierImageItem($id);
		}
		else{
			(new VueCreateur($id))->render(VueCreateur::PageModifierImageItemOnline);
		}
	}
	
	/**
	 * Methode de modification d'image d'item par URL.
	 *
	 * Methode permettant de modifier de l'image d'un item avec une URL.
	 * L'URL doit mener vers une image valide.
	 *
	 * Methode permettant de modifier de l'image d'un item avec une URL.
	 * L'URL doit mener vers une image valide.
	 * L'URL doit se situer dans le $_POST sous le nom de 'Url_Image_Item_Online'.
	 *
	 * @param int[$id] ID dans la base de donnees de l'item dont on veut changer l'image.
	 */
	public function ModifierImageItem($id){
		$item = Item::find($id);
		$url = \Slim\Slim::getInstance()->request()->post('Url_Image_Item_Online');
		//si c'est bien une url :
		if( filter_var( $url, FILTER_VALIDATE_URL)){
			$url = filter_var( $url, FILTER_SANITIZE_URL);
			
			/*VERIFICATION DE S'IL S'AGIT BIEN D'UNE IMAGE VALIDE*/
			/*$estImage = false;
				$tailleImage = @getimagesize($url);
				$estImage = (strtolower(substr($tailleImage['mime'], 0, 5)) == 'image' ? true : false);
			/*fin verification*/
			
			if (true) { //c'est une image
				
				$item->img = $url;
				$item->save();
				//retour à la page
				$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id' => $item->id]);
				header("Location: ".$url);
				exit();
			}
			else { //ce n'est pas une image
				//retour à la page
				$url = \Slim\Slim::getInstance()->urlFor('modifier_image_item_online', ['id' => $item->id]);
				header("Location: ".$url);
				exit();
			}
		}
		else{
			(new VueCreateur($id))->render(VueCreateur::PageModifierImageItemOnline);
		}

	}
	
	/**
	 * Methode d'affichage de la page de modification d'image d'item par transfert de fichier.
	 *
	 * Methode permettant d'afficher la page de modification de l'image d'un item en transferant l'image sur le site.
	 * Dans cette page devra etre envoye par le client un fichier image valide au serveur.
	 *
	 * Methode permettant d'afficher la page de modification de l'image d'un item en transferant l'image sur le site.
	 * Dans cette page devra etre envoye par le client un fichier image valide au serveur.
	 *
	 * @param int[$id] ID dans la base de donnees de l'item dont on veut changer l'image.
	 */
	public function AffichageModifierImageItemInterne($id){
		$app = \Slim\Slim::getInstance();
		if(isset($_POST['uploaderImage_validation']) and $app->request()->post('uploaderImage_validation')=='EnvoyerLeFichier' ){
			$this->AjouterImageInterne($id);
		}
		else{
			(new VueCreateur($id))->render(VueCreateur::PageModifierImageItemInterne);
		}
	}
	
	/**
	 * Methode de modification d'image d'item par transfert de fichier.
	 *
	 * Methode permettant de modifier de l'image d'un item avec un fichier transfere sur le serveur.
	 * Le fichier doit etre un fichier image valide.
	 *
	 * Methode permettant de modifier de l'image d'un item avec un fichier transfere sur le serveur.
	 * Le fichier doit etre un fichier image valide.
	 * Le fichier doit se situer dans $_FILES['new_Image'].
	 *
	 * Cette methode met egalement a jour la base de donnees pour que le fichier transfere soit stocke dedans,
	 * dans la table associee aux items.
	 *
	 * @param int[$id] ID dans la base de donnees de l'item dont on veut changer l'image.
	 */
	public function AjouterImageInterne($id){
		$app = \Slim\Slim::getInstance();
		$item = Item::find($id);
		
		//on vérifie que l'upload est bien la !
		
		if(isset($_FILES['new_Image'])){
			$titre = $_FILES['new_Image']['name'];
			//réécriture du nom du fichier si besoin pour éviter les doublons
			while(file_exists ( getcwd()."/img/".$titre )){
				$taille = strlen($titre);
				$titre = substr($titre, 0, $taille-4);
				$titre.='_.jpg';
			}
			$destination =  $app->request()->getRootUri();
			//Si on peut upload on upload et on renomme
			if(move_uploaded_file($_FILES['new_Image']['tmp_name'], getcwd()."/img/".$titre) ){
				/*verification de la validité de l'image*/
				$estImage = false;
				$tailleImage = @getimagesize(getcwd()."/img/".$titre);
				$estImage = (strtolower(substr($tailleImage['mime'], 0, 5)) == 'image' ? true : false);
				/*fin verification*/
				if ($estImage) { //c'est une image
					$item->img = $titre;
					$item->save();
					//retour à la page
					$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id' => $item->id]);
					header("Location: ".$url);
					exit();
				}
				else { //ce n'est pas une image
					//on supprime le fichier qu'on vient d'acquérir
					unlink(getcwd()."/img/".$titre);
					//retour à la page
					$url = \Slim\Slim::getInstance()->urlFor('modifier_image_item_interne', ['id' => $item->id]);
					header("Location: ".$url);
					exit();
				}
			}
			
		}
		//Que ca marche ou pas on revient sur la page !
		$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id' => $item->id]);
		header("Location: ".$url);
		exit();
			
	}
	
	/**
	 * Methode pour effacer une image de la base de donnees.
	 *
	 * Methode permettant d'effacer une image de la base de donnees
	 * et de la remplacer par random.jpg.
	 *
	 * Methode permettant d'effacer une image de la base de donnees
	 * et de la remplacer par random.jpg, une image par defaut presente de base dans le site.
	 *
	 * @param int[$id] ID de l'item dans la base de donnees dont on efface l'image.
	 * @domain public
	 */
	public function effacerImageItem($id){
		$item = Item::find($id);
		$item->img = 'random.jpg';
		$item->save();
		
		$url = \Slim\Slim::getInstance()->urlFor('afficher_item', ['id' => $item->id]);
		header("Location: ".$url);
		exit();
	}
	
	
	/**
	 * Methode pour supprimer un item precis.
	 *
	 * Methode pour supprimer un item precis d'une liste.
	 * Seul le createur de l'item peut utiliser cette methode.
	 *
	 * Methode pour supprimer un item precis d'une liste.
	 * Seul le createur de l'item peut utiliser cette methode.
	 * L'item, une fois supprime, n'est plus accessible.
	 *
	 * @param int[$id] ID de l'item dans la base de donnee que l'on souhaite supprimer.
	 * @domain public
	 */
	public function SupprimerItem($id){
		$item = Item::find($id);
		$noListe = (Liste::find($item->liste_id))->no;
		$item->delete();
		(new ControleurListe)->afficherListe($noListe);
	}

	/**
	 * Methode permettant d'ouvrir une cagnotte sur un item.
	 *
	 * Methode permettant d'ouvrir une cagnotte sur un item.
	 * Une fois la cagnotte ouverte, il n'est plus possible de la fermer.
	 *
	 * Methode permettant d'ouvrir une cagnotte sur un item.
	 * Une fois la cagnotte ouverte, il n'est plus possible de la fermer.
	 *
	 * @param int[$id] ID de l'item dont on souhaite ouvrir une cagnotte.
	 * @domain public
	 */
	public function ouvrirCagnotte($id){
		$app = \Slim\Slim::getInstance();
		$item = Item::find($id);
		if($item != null){
			if(isset($_SESSION['profil'])){
				if($item->list()->first()->user_id === $_SESSION['profil']['user_id']){
					$item->cagnotte = 1;
					$item->save();
					$app->redirect($app->urlFor('afficher_item', ['id'=>$id]));
				}
			}
		}
		$app->redirect($app->urlFor('erreur'));
	}

	/**
	 * Methode permettant de participer a une cagnotte.
	 *
	 * Methode permettant de participer a une cagnotte.
	 * La participation a la cagnotte ne peut se faire que si la cagnotte n'est pas pleine.
	 * Il est necessaire qu'une cagnotte soit ouverte sur l'item pour y participer.
	 *
	 * Methode permettant de participer a une cagnotte.
	 * La participation a la cagnotte ne peut se faire que si la cagnotte n'est pas pleine.
	 * Il est necessaire qu'une cagnotte soit ouverte sur l'item pour y participer.
	 *
	 * @param int[$id] ID de l'item dont on souhaite participer a la cagnotte.
	 * @param string[$nom] Nom du participant a la cagnotte.
	 * @param float[$montant] Montant de la participation a la cagnotte.
	 * @param string[$message] Message potentiel associe a la participation a la cagnotte.
	 */
	public function participationCagnotte($id, $nom, $montant, $message){
		$item = Item::find($id);
		$app = \Slim\Slim::getInstance();
		if($item != null and $nom != null and $montant != null){
			if($montant > 1){
				$montantRestant = $item->tarif - $item->montantCagnotte();
				if($montant > $montantRestant){
					$montant = $montantRestant;
				}
				$reservation = new \mywishlist\models\Reservation();
				$reservation->item_id = $id;
				$reservation->nom = $nom;
				$reservation->montant = $montant;
						
				if (isset($message)) {
					$reservation->message = $message;
				}
				
				$reservation->save();
				$token = "";
				if(!is_null($app->request->get('token'))){
					$token = "?token=".$app->request->get('token');
				}
				$app->redirect($app->urlFor('afficher_item', ['id'=>$id]).$token);
			}
		}
		$app->redirect($app->urlFor('erreur'));
	}
	
	/**
	 * Methode permettant de reserver un item.
	 *
	 * Methode permettant de reserver un item.
	 * Il est necessaire qu'une cagnotte ne soit pas ouverte sur l'item pour le reserver.
	 *
	 * Methode permettant de participer a une cagnotte.
	 * Il est necessaire qu'une cagnotte ne soit pas ouverte sur l'item pour le reserver.
	 *
	 * @param int[$id] ID de l'item que l'on souhaite reserver.
	 * @param string[$nom] Nom du reserveur.
	 * @param string[$message] Message potentiel associe a la reservation.
	 */
	public function reserverItem($id, $nom, $message) {
		$app = \Slim\Slim::getInstance();
		
		if (isset($id)) {
			$item = \mywishlist\models\Item::find($id);
			if($item === null){
				//Ce n'est pas le cas
				$app->halt(404, 'No item found');
			}
			else{
				if (isset($nom)) {
					setcookie("nom_reserveur", $nom, time() + 60*60*24*7, $app->urlFor('route_defaut')) ;
					//C'est bon on peut commencer a travailler
					//On commence par regarder si l'item est deja reserver
					if (count($item->reservations()->get())==0) {
						//C'est le cas, on peut faire la reservation
						
						$r = new \mywishlist\models\Reservation();
						$r->item_id = $id;
						$r->nom = $nom;
						
						if (isset($message)) {
							$r->message = $message;
						}
						
						$r->save();

					}
					//else l'item est deja reserve, on ne fait rien
					
					//on redirige vers la page de l'item pour voir l'etat de sa reservation
					$token = "";
					if(!is_null($app->request->get('token'))){
						$token = "?token=".$app->request->get('token');
					}
					$url = $app->urlFor('afficher_item', ['id' => $item->id]).$token;
					$app->redirect($url);
				}
				else {
					//Il n'y a pas de nom
					$app->halt(404, 'No name gived');
				}
			}
		}
		else {
			//Il n'y a pas d'ID
			$app->halt(404, 'No item found');
		}
	}
}