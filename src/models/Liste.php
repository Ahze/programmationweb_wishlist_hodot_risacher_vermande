<?php
namespace mywishlist\models;

use \mywishlist\models\Message;

/**
 * Classe représentant une Liste pouvant être géré par la base de donnée.
 *
 * Classe représentant une Liste pouvant être géré par la base de donnée
 * en utilisant Eloquent\Model.
 *
 * Classe représentant une Liste pouvant être géré par la base de donnée
 * en utilosant Eloquent\Model. De nombreuses fonction sont implémentées
 * dans le classe pour gerer le plus simplement la liasion entre la base 
 * de données, le code PHP et le rendu HTML.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class Liste extends
		\Illuminate\Database\Eloquent\Model{
	
	protected $table ='liste';
	protected $primaryKey = 'no';
	public $timestamps = false;
	
	public function items()
	{
		return $this->hasMany('mywishlist\models\Item','liste_id');
	}
	
	public function messages(){
		return $this->hasMany('mywishlist\models\Message','liste_id');
	}

	public function estPublique(){
		return $this->token == null;
	}

	public function estExpiree(){
		$now = date('Y-m-d');
		$now = strtotime($now);
		$timeListe = strtotime($this->expiration);
		return ($timeListe <= $now);
	}

	public function asCard(){
		$url = \Slim\Slim::getInstance()->urlFor('afficher_liste', ['no'=> $this->no]);
		$desc = substr($this->description, 0, 50);
		if(strlen($this->description) > 50){
			$desc .= "...";
		}
		return <<<END
			<div class="card carte-liste">
  				<div class="card-body centered">
    				<h5 class="card-title">$this->titre</h5>
    				<h6 class="card-subtitle mb-2 text-muted">Expire le $this->expiration</h6>
    				<p class="card-text">$desc</p>
			    		<a href="$url" class="card-link btn btn-secondary">Acceder</a>
  				</div>
			</div>
END;
	}
	
	public function toutMessageListe(){
		$tab_Message = $this->messages()->get();
		$res = <<<END
		<div class="row" style="margin-top:5%;">
			<div class="col-3"></div>
			<div class="col-6 centered">
				<ul class="list-group">
END;
		foreach($tab_Message as $message){
			$res.=<<<END
					<li class="list-group-item">$message->texte</li>
END;
		}
		$res .= <<<END
				</ul>
			</div>
			<div class="col-3"></div>
		</div>
END;
		return $res;
	}

	public function utilisateur(){
		return $this->belongsTo('mywishlist\models\Utilisateur', 'user_id');
	}

	public function supprimer(){
		foreach ($this->items()->get() as $obj) {
			$obj->supprimer();
		}
		foreach ($this->messages()->get() as $m) {
			$m->delete();
		}
		$this->delete();
	}
}