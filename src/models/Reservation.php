<?php
namespace mywishlist\models;

/**
 * Classe représentant une reservation associé à un item pouvant être géré par la base de donnée.
 *
 * Classe représentant une reservation associé à un item pouvant être géré par la base de donnée
 * en utilisant Eloquent\Model.
 *
 * Classe représentant une reservation associé à un item pouvant être géré par la base de donnée.
 * en utilisant Eloquent\Model pour faire le lien avec phpMyAdmin et MySql. 
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class Reservation extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'reservation';
	protected $primaryKey = 'id' ;
	public $timestamps = false ;

	/**
	* Methode qui retourne la reservation de l'objet.
	*
	* Methode qui retourne la reservation de l'objet en utilisant Eloquent\Model.
	*
	* Methode qui retourne la reservation de l'objet en utilisant Eloquent\Model
	* et la fonction belongsTo.
	*/
	public function item(){
		return $this->belongsTo('\mywishlist\models\Item', 'item_id');
	}
}