<?php
namespace mywishlist\models;

/**
 * Classe représentant un message associé à une liste pouvant être géré par la base de donnée.
 *
 * Classe représentant un message associé à une liste pouvant être géré par la base de donnée
 * en utilosant Eloquent\Model.
 *
 * Classe représentant un message associé à une liste pouvant être géré par la base de donnée
 * en utilisant Eloquent\Model pour faire le lien avec phpMyAdmin et MySql.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class Message extends
		\Illuminate\Database\Eloquent\Model{
	
	protected $table ='message';
	protected $primaryKey = 'message_id';
	public $timestamps = false;

}