<?php

namespace mywishlist\models;

/**
 * Classe représentant une autentification pouvant être géré par la base de donnée.
 *
 * Classe représentant une autentification pouvant être géré par la base de donnée
 * en utilosant Eloquent\Model.
 *
 * Classe représentant une autentification pouvant être géré par la base de donnée
 * en utilisant Eloquent\Model. De nombreuses fonction sont implémentées dans le 
 * classe pour gerer le plus simplement la liasion entre la base de données, le 
 * code PHP et le rendu HTML ainsi que la connexion d'un utilisateur au site.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class Authentification{

	/**
	 * Methode permettant d'ajouter un utilisateur à la base de donnée.
	 *
	 * Methode permettant d'ajouter un utilisateur à la base de donnée
	 * à partir des parametres de cette derniere.
	 *
	 * Methode permettant d'ajouter un utilisateur à la base de donnée
	 * à partir des parametres suivants :
	 * le nom de l'utilisateur (string),
	 * l'adresse mail de l'utilisateur (string),
	 * le mot de passe de l'utilisateur(string).
	 *
	 * @param string[$nom] Nom de l'utilisateur dans la base de données.
	 * @param string[$email] adresse mail de l'utilisateur dans la base de données.
	 * @param string[$mdp] mot de passe de l'utilisateur dans la base de données.
	 * @domain public
	 */
	public static function creerUtilisateur($nom, $email, $mdp){
		if(Utilisateur::where('email', '=', $email)->first() != null){//email deja prise
			setcookie('erreur_email', "L'adresse email est déjà utilisée", time()+60);
			return false;
		}
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			setcookie('erreur_email', "L'adresse email n'est pas valide", time()+60);
			return false;
		}
		$user = new Utilisateur();
		$user->nom = $nom;
		$user->email = $email;
		$user->mdp = password_hash($mdp, PASSWORD_DEFAULT, ['cost'=>12]);
		$user->save();
		self::chargerProfil($user->id);
		return true;
	}

	/**
	 * Methode permettant à un utilisateur de s'autentifier.
	 *
	 * Methode permettant à un utilisateur de s'autentifier
	 * à partir des son mail et de son mot de passe.
	 *
	 * Methode permettant à un utilisateur de s'autentifier
	 * à partir des parametres suivants :
	 * l'adresse mail de l'utilisateur (string),
	 * le mot de passe de l'utilisateur (string).
	 *
	 * @param string[$email] adresse mail entrée par l'utilisateur.
	 * @param string[$mdp] mot de passe entrée par l'utilisateur.
	 * @domain public
	 */
	public static function authentifier($email, $mdp){
		$user = Utilisateur::where('email', '=', $email)->first();
		if($user === null){
			setcookie('connexion_ratee', "L'adresse email n'existe pas", time()+60);
			return false;
		}
		if(password_verify($mdp, $user->mdp)){
			self::chargerProfil($user->id);
			return true;
		}
		else{
			setcookie('connexion_ratee', "Mot de passe erroné", time()+60);
			return false;
		}
	}

	/**
	 * Methode permettant de charger un profil utilisateur.
	 *
	 * Methode permettant de charger un profil utilisateur
	 * et de la charger en variable de session.
	 * 
	 * Methode permettant de charger un profil utilisateur
	 * et de la charger en variable de session à partir de
	 *
	 * ID du pprofil à charger (int).
	 *
	 * @param int[$id] ID du profil utiilisateur à charger.
	 * @domain public
	 */
	private static function chargerProfil($id){
		$user = Utilisateur::find($id);
		$_SESSION['profil'] = array(
		'username' => $user->nom, 
		'user_id' => $id,
		'auth-level' => 1);
	}

	/**
	 * Methode permettant de savoir si un utilisateur à des droits d'acces.
	 *
	 * Methode permettant de savoir si un utilisateur à des droits d'acces
	 * en vérifiant la variable de session actuelle.
	 * 
	 * Methode permettant de savoir si un utilisateur à des droits d'acces
	 * en vérifiant la variable de session actuelle et du niveau de sécurité 
	 * demandé.
	 * 
	 * niveau de sécurité demandé (int).
	 *
	 * @param int[$required] niveau de sécurité demandé.
	 * @domain public
	 */
	public static function droitAcces($required){
		if(isset($_SESSION['profil'])){
			return $_SESSION['profil']['auth-level'] >= $required;
		}
		return $required < 1;
	}
	
	/**
	* Methode qui déconnecte la session actuelle.
	*
	* Methode qui déconnecte la session actuelle, il n'y a donc plus de profil connécté.
	*
	* Methode qui déconnecte la session actuelle, il n'y a donc plus de profil connécté
	* la fonction utilise la methode unset.
	*/
	public static function deconnexion(){
		unset($_SESSION['profil']);
	}

}