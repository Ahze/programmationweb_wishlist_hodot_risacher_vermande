<?php
namespace mywishlist\models;

/**
 * Classe représentant un Item pouvant être géré par la base de donnée.
 *
 * Classe représentant un Item pouvant être géré par la base de donnée
 * en utilosant Eloquent\Model.
 *
 * Classe représentant un Item pouvant être géré par la base de donnée
 * en utilisant Eloquent\Model. De nombreuses fonction sont implémentées
 * dans le classe pour gerer le plus simplement la liasion entre la base 
 * de données, le code PHP et le rendu HTML.
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class Item extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'item';
	protected $primaryKey = 'id' ;
	public $timestamps = false ;
 
	 /**
	 * Methode retournant la liste a laquelle appartient l'item.
	 *
	 * Methode retournant la liste a laquelle appartient l'item en utilisant Eloquent\Model.
	 *
	 * Methode retournant la liste a laquelle appartient l'item en utilisant Eloquent\Model 
	 * et la fonction belongsTo.
	 */
	public function list(){
		return $this->belongsTo('\mywishlist\models\Liste', 'liste_id');
	}
	
	public function reservations()
	{
		return $this->hasMany('mywishlist\models\Reservation', 'item_id');
	}

	public function reservation()
	{
		return $this->hasOne('mywishlist\models\Reservation', 'item_id')->first();
	}

	public function estReserve(){
		if($this->cagnotte){
			return count($this->reservations()->get()) > 0;
		}
		else{
			return !is_null($this->reservation());
		}
	}

	public function montantCagnotte(){
		$montant = 0;
		foreach ($this->reservations()->get() as $participationCagnotte) {
			$montant += $participationCagnotte->montant;
		}
		return $montant;
	}

	/*
	1 : affichage de la liste par un utilisateur
	2 : affichage pour le createur (lorque la liste est expiree)
	*/
	public function texteReservation($afficherReservation = 0){
		$txtReservation = "";
		switch($afficherReservation){
			case 1: //affichage de la liste par un utilisateur
				if($this->cagnotte){
					if($this->estReserve()){
						$txtReservation = "Participant(s) de la gagnotte :<br>";
						foreach ($this->reservations()->get() as $reservation) {
							$txtReservation .= $reservation->nom." ".$reservation->montant."<br>";
						}
					}
					else{
						$texteReservation = "Cagnotte ouverte";
					}
				}
				else{
					if($this->estReserve()){
						$txtReservation = "Réservé par ".$this->reservation()->nom;
					}
					else{
						$txtReservation = "N'a toujours pas été réservé";	
					}
				}
				break;
			case 2://affichage pour le createur (lorque la liste est expiree)
				if($this->cagnotte){
						if($this->estReserve()){
							$txtReservation = "Participant(s) de la gagnotte :<br>";
							foreach ($this->reservations()->get() as $reservation) {
								$txtReservation .= $reservation->nom."<br>";
								if ($reservation->message!="")
									$txtReservation .= "\"".$reservation->message."\"<br>";
							}
						}
						else{
							$texteReservation = "Personne n'a participé à la cagnotte";
						}
					}
					else{
						if($this->estReserve()){
							$txtReservation = "Réservé par ".$this->reservation()->nom;
							if ($this->reservation()->message!="")
								$txtReservation .= "<br>\"".$this->reservation()->message."\"";
						}
						else{
							$txtReservation = "N'a pas été réservé";	
						}
					}
					break;

		}
		return $txtReservation;
	}

	 /**
	 * Methode permettant la supprimer un item.
	 *
	 * Methode permettant la supprimer un item de la base de donnée.
	 *
	 * Methode permettant la supprimer un item de la base de donnée
	 * en supprimant la reservation associée en utilisant la methode delete.
	 */
	public function supprimer(){
		foreach ($this->reservations()->get() as $r) {
			$r->delete();
		}
		$this->delete();
	}

}