<?php
namespace mywishlist\models;

/**
 * Classe représentant un utilisateur du site pouvant être géré par la base de donnée.
 *
 * Classe représentant un utilisateur du site pouvant être géré par la base de donnée
 * en utilisant Eloquent\Model.
 *
 * Classe représentant un utilisateur du site pouvant être géré par la base de donnée
 * en utilisant Eloquent\Model pour faire le lien avec phpMyAdmin et MySql. 
 *
 * @author S3B_HODOT_VERMANDE_RISACHER
 */
class utilisateur extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'utilisateur';
	protected $primaryKey = 'id' ;
	public $timestamps = false ;
	
	/**
	* Methode qui retourne les listes de l'utilisateurs.
	*
	* Methode qui retourne les listes de l'utilisateurs avec Eloquent\Model.
	* 
	* Methode qui retourne les listes de l'utilisateurs avec Eloquent\Model
	* et en utilisant la finction hasMany.
	*/
	public function lists()
	{
		return $this->hasMany('mywishlist\models\Liste','user_id');
	}

	/**
	* Methode permettant de supprimer un utilisateur.
	*
	* Methode Methode permettant de supprimer un utilisateur de la base de données.
	*
	* Methode Methode permettant de supprimer un utilisateur de la base de données
	* a partir de la fonction supprimer de liste.
	*/
	public function supprimer(){
		foreach ($this->lists()->get() as $l) {
			$l->supprimer();
		}
		$this->delete();
	}
}