SET NAMES utf8;
SET time_zone = '+00:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `reservation`;
DROP TABLE IF EXISTS `message`;
DROP TABLE IF EXISTS `item`;
DROP TABLE IF EXISTS `liste`;
DROP TABLE IF EXISTS `utilisateur`;

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(40) NOT NULL,
  `email` VARCHAR(40) NOT NULL,
  `mdp` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`id`)
);





CREATE TABLE `liste` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `expiration` date NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no`),
  FOREIGN KEY (`user_id`) REFERENCES utilisateur(`id`)

);


CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `liste_id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text,
  `img` text,
  `url` text,
  `tarif` decimal(5,2) DEFAULT NULL,
  `cagnotte` boolean NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`liste_id`) REFERENCES liste(`no`)
);



CREATE TABLE `reservation` (
  `id` int(11) AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `nom` VARCHAR(40) NOT NULL,
  `montant` decimal(5,2) DEFAULT NULL,
  `message` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`item_id`) REFERENCES item(`id`)
);


CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `liste_id` int(11) NOT NULL,
  `texte` text DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  FOREIGN KEY (`liste_id`) REFERENCES liste(`no`)
);


#mdp : salut
INSERT INTO `utilisateur` (`id`, `nom`, `email`, `mdp`) VALUES (1, 'James', 'james@yahoo.fr', '$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO');
#mdp : bonjour	
INSERT INTO `utilisateur` (`id`, `nom`, `email`, `mdp`) VALUES (2, 'John', 'john@yahoo.fr', '$2y$12$SEzkuW.ceHFka/co1Pcttec3sSy1NIjYpBhVBwRaZFlz5A7yWFTwO');
#mdp : yo
INSERT INTO `utilisateur` (`id`, `nom`, `email`, `mdp`) VALUES (3, 'Paul', 'paul@yahoo.fr', '$2y$12$6cp2WrIlqScd4F0XpQiGH.KQNnf8cP/EFbDgK4cu7fsheVknlWLFu');


INSERT INTO `liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `token`) VALUES
(1, 1,  'Pour fêter le bac !',  'Pour un week-end à Nancy qui nous fera oublier les épreuves. ',  '2018-06-27', 'nosecure1'),
(2, 2,  'Liste de mariage d\'Alice et Bob', 'Nous souhaitons passer un week-end royal à Nancy pour notre lune de miel :)',  '2018-06-30', 'nosecure2'),
(3, 3,  'C\'est l\'anniversaire de Charlie',  'Pour lui préparer une fête dont il se souviendra :)',  '2017-12-12', 'nosecure3');



INSERT INTO `item` (`id`, `liste_id`, `nom`, `descr`, `img`, `url`, `tarif`) VALUES
(1,	2,	'Champagne',	'Bouteille de champagne + flutes + jeux à gratter',	'champagne.jpg',	'',	20.00),
(2,	2,	'Musique',	'Partitions de piano à 4 mains',	'musique.jpg',	'',	25.00),
(3,	2,	'Exposition',	'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel',	'poirelregarder.jpg',	'',	14.00),
(4,	3,	'Goûter',	'Goûter au FIFNL',	'gouter.jpg',	'',	20.00),
(5,	3,	'Projection',	'Projection courts-métrages au FIFNL',	'film.jpg',	'',	10.00),
(6,	2,	'Bouquet',	'Bouquet de roses et Mots de Marion Renaud',	'rose.jpg',	'',	16.00),
(7,	2,	'Diner Stanislas',	'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)',	'bonroi.jpg',	'',	60.00),
(8,	3,	'Origami',	'Baguettes magiques en Origami en buvant un thé',	'origami.jpg',	'',	12.00),
(9,	3,	'Livres',	'Livre bricolage avec petits-enfants + Roman',	'bricolage.jpg',	'',	24.00),
(10,	2,	'Diner  Grand Rue ',	'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)',	'grandrue.jpg',	'',	59.00),
(11,	1,	'Visite guidée',	'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas',	'place.jpg',	'',	11.00),
(12,	2,	'Bijoux',	'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil',	'bijoux.jpg',	'',	29.00),
(19,	1,	'Jeu contacts',	'Jeu pour échange de contacts',	'contact.png',	'',	5.00),
(22,	1,	'Concert',	'Un concert à Nancy',	'concert.jpg',	'',	17.00),
(23,	1,	'Appart Hotel',	'Appart’hôtel Coeur de Ville, en plein centre-ville',	'apparthotel.jpg',	'',	56.00),
(24,	2,	'Hôtel d\'Haussonville',	'Hôtel d\'Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas',	'hotel_haussonville_logo.jpg',	'',	169.00),
(25,	1,	'Boite de nuit',	'Discothèque, Boîte tendance avec des soirées à thème & DJ invités',	'boitedenuit.jpg',	'',	32.00),
(26,	1,	'Planètes Laser',	'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.',	'laser.jpg',	'',	15.00),
(27,	1,	'Fort Aventure',	'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l\'élastique inversé, Toboggan géant... et bien plus encore.',	'fort.jpg',	'',	25.00);


