<?php

session_start();

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use mywishlist\controlers\ControleurItem;
use mywishlist\controlers\ControleurListe;
use mywishlist\controlers\ControleurUtilisateur;
use mywishlist\vue\VueParticipant;
use mywishlist\vue\VueCreateur;
use \mywishlist\models\Authentification as Auth;

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

	//Par défault
	$app->get('/', function() {
		if(Auth::droitAcces(1)){
			$vue = new VueCreateur(Array());
			$vue->render(VueCreateur::PageDeBase);
		}
		else {
			$vue = new VueParticipant(Array());
			$vue->render(VueParticipant::PageDeBase);
		}
	})->name('route_defaut');

	$app->get('/error', function(){
		$v = new VueParticipant(null);
		$v->render(VueParticipant::ContenuInaccessible);
	})->name('erreur');

	//Incription et connexion
	$app->get('/connexion', function(){
		$c = new ControleurUtilisateur();
		$c->afficherPageConnexion();
	})->name('connexion');

	//Se connecter
	$app->post('/connexion', function(){
		$app = \Slim\Slim::getInstance();
		$c = new ControleurUtilisateur();
		$c->essayerConnexion($app->request->post('email'), $app->request->post('mdp'));
	});
	//e deconnecter
	$app->get('/deconnexion', function(){
		$app = \Slim\Slim::getInstance();
		if(Auth::droitAcces(1)){	
			Auth::deconnexion();
		}
		$app->redirect($app->urlFor('route_defaut'));
	})->name('deconnexion');

	//S'inscrire
	$app->get('/inscription', function(){
		$c = new ControleurUtilisateur();
		$c->afficherPageInscription();
	})->name('inscription');

	$app->post('/inscription', function(){
		$c = new ControleurUtilisateur();
		$app = \Slim\Slim::getInstance();
		$nom = $app->request->post('nom');
		$prenom = $app->request->post('prenom');
		$email = $app->request->post('email');
		$mdp = $app->request->post('mdp');
		$c->envoyerMailInscription($nom, $prenom, $email, $mdp);
	});

	$app->get('/modifier-compte', function(){
		if(Auth::droitAcces(1)){
			$c = new ControleurUtilisateur();
			$c->afficherModificationCompte();
		}
	})->name('modification_compte');

	$app->post('/modifier-compte', function(){
		if(Auth::droitAcces(1)){
			$c = new ControleurUtilisateur();
			$c->modifierCompte();
		}
	});

	$app->get('/changer-mdp', function(){
		if(Auth::droitAcces(1)){
			$c = new ControleurUtilisateur();
			$c->afficherModificationMotDePasse();
		}
	})->name('changer_mdp');

	$app->post('/changer-mdp', function(){
		if(Auth::droitAcces(1)){
			$c = new ControleurUtilisateur();
			$c->modifierMotDePasse();
		}
	});

	$app->get('/supprimer-compte', function(){
		if(Auth::droitAcces(1)){
			$c = new ControleurUtilisateur();
			$c->supprimerCompte();
		}
	})->name('supprimer_compte');

	//Afficher un item dont on donne l'idée
	$app->get('/item/:id', function($id) {
		$ctr = new ControleurItem();
		$item = $ctr->afficherItem($id);
	})->name('afficher_item');

	//Creer une liste
	$app->get('/liste/creer', function(){
		if(Auth::droitAcces(1)){
			(new ControleurListe())->afficherCreationListe();
		}
	})->name('creer_liste');

	$app->post('/liste/enregistrer', function(){
		if(Auth::droitAcces(1)){
			$c = new ControleurListe();
			$list = $c->enregistrerListe();
			$url = \Slim\Slim::getInstance()->urlFor('afficher_liste', ['no' => $list->no]);
			\Slim\Slim::getInstance()->redirect($url);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('enregistrer_liste');

	
	//Affiche le contenue d'une liste dont on donne l'id
	$app->get('/liste/:no', function($no) {  
		$liste = new mywishlist\controlers\ControleurListe();
		$liste->afficherListe($no);
	})->name('afficher_liste');

	//rendre une liste publique
	$app->post('/publier/:no', function($no){
		if(Auth::droitAcces(1)){
			$c = new ControleurListe();
			$c->publierListe($no);
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor('afficher_liste', ['no'=>$no]));
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('publier_liste_vers_publique');
	
	
	//AFFICHER LES LISTES EN FONCTION DE CRITERES PRECIS !!!!!!!!!!!!!!!!!!!!!!!!!!
	//Afficher toutes les listes
	$app->get('/listes', function() {
		(new ControleurListe())->afficherListes();
	})->name('afficher_listes');
	
	//afficher toutes les listes publiques
	$app->get('/listesPubliques', function(){
		(new ControleurListe())->afficherListesPubliques();	
	})->name('afficher_listes_publiques');
	//afficher toutes les listes persos
	$app->get('/listesPersos', function(){
		if(Auth::droitAcces(1)){
			(new ControleurListe())->afficherListesPersos();	
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('afficher_listes_persos');
	
	//FIN DE LA ZONE EN TRAVEAU !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	//Modifier Une liste éxistante
	$app->get('/liste/modifier/:no', function($no) {
		if(Auth::droitAcces(1)){
			(new ControleurListe())->afficherModificationListe($no);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('modifier_Liste');

	$app->post('/liste/modifier/:no', function($no) {
		if(Auth::droitAcces(1)){
			$c = new ControleurListe();
			$c->ModifierListe($no);
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor('afficher_liste', ['no'=>$no]));
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	});
	
	//Ajouter un Item à une liste
	$app->get('/item/ajouter/:idList', function($idList){
		if(Auth::droitAcces(1)){
			(new ControleurItem())->afficherAjoutItemAListe($idList);	
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('ajouter_item_a_liste');

	$app->post('/item/ajouter/:idList', function($idList){
		if(Auth::droitAcces(1)){
			(new ControleurItem())->afficherAjoutItemAListe($idList);	
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	});
	
	//Modifier un Item
	$app->get('/item/modifier/:id', function($id){
		if(Auth::droitAcces(1)){
			(new ControleurItem())->afficherModificationItem($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('modifier_Item');

	$app->post('/item/modifier/:id', function($id){
		if(Auth::droitAcces(1)){
			(new ControleurItem())->afficherModificationItem($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	});
	
	//Supprimer Un Item
	$app->get('/item/supprimer/:id', function($id){
		if(Auth::droitAcces(1)){
			(new ControleurItem())->SupprimerItem($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('supprimer_item_liste');

	//ouvrir une cagnotte pour un item
	$app->post('/ouvrir-cagnotte/:id', function($id){
		if(Auth::droitAcces(1)){
			$c = new ControleurItem();
			$c->ouvrirCagnotte($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('ouvrir_cagnotte');

	$app->post('/participer-cagnotte/:id', function($id){
		$c = new ControleurItem();
		$app = \Slim\Slim::getInstance();
		$nom = "";
		if (isset($_SESSION['profil']))
			$nom = $_SESSION['profil']['username'];
		else
			$nom = $app->request->post('nomParticipant');
		$montant = $app->request()->post('montantParticipation');
		$texte = $app->request()->post('texteReservation');
		$c->participationCagnotte($id, $nom, $montant, $texte);
	})->name('participer_cagnotte');
	
	//Supprimer Une Liste
	$app->get('/liste/supprimer/:no', function($no){
		if(Auth::droitAcces(1)){
			(new ControleurListe())->supprimerListe($no);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('supprimer_liste');

	//Réserver Un Item
	$app->post('/reserver/:id', function($id){
		$c = new ControleurItem();
		$app = \Slim\Slim::getInstance();
		$nom = "";
		if (isset($_SESSION['profil']))
			$nom = $_SESSION['profil']['username'];
		else
			$nom = $app->request->post('nomParticipant');
		$texte = $app->request->post('texteReservation');
		$c->reserverItem($id, $nom, $texte);
	})->name('reserver_item');
	
	//Changer les images d'items  avec url en ligne
	$app->get('/item/modifier/image/online/:id', function($id){
		if(Auth::droitAcces(1)){
			(new ControleurItem())->AffichageModifierImageItemOnline($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('modifier_image_item_online');

	$app->post('/item/modifier/image/online/:id', function($id){
		if(Auth::droitAcces(1)){
			(new ControleurItem())->ModifierImageItem($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	});
	
	//Changer les images d'items avec un import interne a la BDD
	$app->get('/item/modifier/image/interne/:id', function($id) {
		if(Auth::droitAcces(1)){
			(new ControleurItem())->AffichageModifierImageItemInterne($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	})->name('modifier_image_item_interne');

	$app->post('/item/modifier/image/interne/:id', function($id) {
		if(Auth::droitAcces(1)){
			(new ControleurItem())->AjouterImageInterne($id);
		}
		else{
			(new VueParticipant(NULL))->render(VueParticipant::PageDeBase);
		}
	});
	
	//Supprimer l'image d'un item
	$app->get('/item/modifier/image/supprimer/:id', function($id){
		$app = \Slim\slim::getInstance();
		if(Auth::droitAcces(1)){
			(new ControleurItem())->effacerImageItem($id);
		}
		else{
			$app->redirect($app->urlFor('erreur'));
		}
	})->name('supprimer_image_item');
	
	//Expirer liste
	$app->get('/expirer-liste/:id', function($id){
		$app = \Slim\slim::getInstance();
		if(Auth::droitAcces(1)){
			(new ControleurListe())->ExpirerListe($id);
			$app->redirect($app->urlFor('afficher_liste', ['id'=>$id]));
		}
		else{
			$app->redirect($app->urlFor('erreur'));
		}
	})->name('expirer_liste');
	
	//ajouter un message
	$app->post('/liste/message/ajouter/:id', function($id){
		(new ControleurListe())->ajouterMessage($id);
	})->name('ajouter_Message_liste');

$app->run();
